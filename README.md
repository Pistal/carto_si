### Carto SI

Génération automatique d'une carte sous forme de graphe, à partir d'un fichier Excel.
Projet réalisé dans le contexte du Last Project à l'Ecole Supérieure d'Ingénieurs Paris-Est.

L'application base sa représentation graphique sur **SigmaJS** :https://github.com/jacomyal/sigma.js/

### Pré-requis développement en local

Paquets nécessaires :
- docker
- docker-compose

Autre fichiers nécessaires:
- Certificats
- Configuration keycloak 

### Initialisation

# TODO
Placer le certificat dans un dossier ``x509`` à la racine du projet

#### Keycloak

Importer la configuration sur la console de l'administration Keycloak



### Développer

Lancez docker-compose avec le fichier `docker-compose.yml` situé à la racine du projet.
Keycloak est nécessaire au développement de l'application, donc il doit être allumé. ( à noter sur les déploiements que keycloak est déployé une seule fois pour tous les environnements ).
Puisque le setup pour lancer l'application est lourde et couteuse, il faut privilégier l'utilisation de plugins Hot-reload pour accélérer le développement.

## Front-end

SigmaJS est exploité depuis ReactJS, et profite de la librairie **react-sigma-v2**.
Graphology est également utilisé afin de gérer la partie données du graphe.

## Back-end

La partie Back-end est exécutée sur une base Spring Boot et intéragit avec une base de données PostgreSQL.

### API REST

Afin d'intéragir avec la partie back de l'application Carto SI, une API est disponible en REST et est totalement générée par swagger-codegen (https://swagger.io/tools/swagger-codegen/).
Cette dépendance se base sur le fichier `src/main/resources/config.yaml` afin de générer:
- Les interfaces `{Route}Api` (UserApi par exemple) fournissant les méthodes Java à implémenter côté back.
- Les Pojos ( ou objets payload, ou DTO )

Ces classes seront générées dans le répertoire `target/generated-sources/` donc il faut bien indiquer à IntelliJ qu'il prenne en compte ces fichiers
en tant que fichiers sources.

## Environnements

Les fichiers application.properties permettent d'avoir des configurations différentes en fonction de l'environnement.

### Ajouter un nouvel environnement

Sur l'instance keycloak partagée entre les environnements, créer un Realm `si-{ENV}` ainsi qu'une ressource `carto-si-{ENV}`.
Ajouter un fichier `application-{ENV}.properties` s'adaptant à ces nouveaux noms de keycloak:
``
keycloak.realm=si-{ENV};
keycloak.resource=carto-si-{ENV};
``
Adapter le CI en ajoutant un job pour qu'il construise l'image sur cet environnement et la déploie dans le registre de gitlab.
Au niveau de déploiement avec le docker-compose.yml, spécifier :

`- SPRING_PROFILES_ACTIVE={ENV}`
Cette variable permet de spécifier sur quel fichier `application{ENV}.properties` l'application va se baser.
**Notez tout de même que le fichier `application.properties` de base est chargé peu importe le profil activé.**

Attention tout de même aux ports utilisés pour cet environnement en prenant en compte la nouvelle base de donnée nécessaire au fonctionnement de l'application CartoSI.