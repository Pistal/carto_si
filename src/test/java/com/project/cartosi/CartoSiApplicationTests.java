package com.project.cartosi;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = CartoSiApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CartoSiApplicationTests {

    @Test
    void contextLoads(){}

}
