package com.project.cartosi.buildingattribute;

import com.project.cartosi.api.CanvasApi;
import com.project.cartosi.building.Building;
import com.project.cartosi.building.BuildingRepository;
import com.project.cartosi.header.HeaderRepository;

import com.project.cartosi.pojos.AttributePOJO;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;


import java.util.*;

/**
 * The type Building attribute controller.
 */
@RestController
public class BuildingAttributeController implements CanvasApi {

    private final BuildingRepository buildingRepository ;
    private final HeaderRepository headerRepository;

    /**
     * Instantiates a new Building attribute controller.
     *
     * @param buildingRepository the building repository
     * @param headerRepository   the header repository
     */
    public BuildingAttributeController(BuildingRepository buildingRepository, HeaderRepository headerRepository) {
        this.buildingRepository = buildingRepository;
        this.headerRepository = headerRepository;
    }

    @Override
    public ResponseEntity<List<AttributePOJO>> getBuildingAttributes(Long buildingId, Long mapId) {
        // Check id building and idmap
        if ( buildingId < 0 || mapId <0 ){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        // Get the building in database
        Optional<Building> buildingOptional = buildingRepository.findById(buildingId);

        //Check if building is empty
        if(buildingOptional.isEmpty()){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        Building building = buildingOptional.get();
        // Check if the buildind and mapId
        Long idMap= building.getDistrict().getDistrictValue().getValue().getAttribute().getConfiguration().getHeader().getId();
        if(idMap == null || idMap < 0 || mapId.equals(idMap)) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        // Get all values
        ArrayList<AttributePOJO> attributes = new ArrayList();
        var values = building.getProperties();
        values.stream().forEach(value -> {
            AttributePOJO attributePojo =new AttributePOJO() ;
            attributePojo.setAttributeValue(value.getName());
            attributePojo.setAttributeKey(value.getAttribute().getName());
            attributes.add(attributePojo);
        });

        return ResponseEntity.ok(attributes);
    }
}
