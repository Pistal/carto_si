package com.project.cartosi.building;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BuildingRepository extends JpaRepository<Building, Long> {
    @Query("SELECT b FROM Building b WHERE b.key.attribute.configuration.id=:configurationId")
    List<Building> getBuildingByConfiguration(Long configurationId);

    @Query("SELECT b FROM Building b JOIN b.properties p WHERE p.attribute.name=:propertyName AND p.name=:propertyValue AND p.attribute.configuration.id=:configurationId")
    Building getBuildingByPropertyAndConfiguration(String propertyName, String propertyValue, Long configurationId);
}