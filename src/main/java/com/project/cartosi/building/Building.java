package com.project.cartosi.building;

import com.project.cartosi.building_value.BuildingValue;
import com.project.cartosi.value.Value;
import com.project.cartosi.district.District;
import com.project.cartosi.link.BuildingLink;

import javax.persistence.*;
import java.util.LinkedHashSet;
import java.util.Set;

@Entity
@Table(name = "building")
public class Building {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_district")
    private District district;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "id_building_value", nullable = false)
    private BuildingValue buildingValue;

    @OneToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "id_key", nullable = false)
    private Value key;

    @Column(name = "size")
    private Integer size;

    @Column(name = "color", length = 50)
    private String color;

    @Column(name = "x_coordinate", nullable = false)
    private Double xCoordinate;

    @Column(name = "y_coordinate", nullable = false)
    private Double yCoordinate;

    @Column(name = "comment", length = 50)
    private String comment;

    @OneToMany(mappedBy = "departingBuilding", cascade = CascadeType.REMOVE, orphanRemoval = true)
    private Set<BuildingLink> departingLinks = new LinkedHashSet<>();

    @OneToMany(mappedBy = "landingBuilding", cascade = CascadeType.REMOVE, orphanRemoval = true)
    private Set<BuildingLink> landingLinks = new LinkedHashSet<>();

    @ManyToMany
    @JoinTable(name = "building_property",
            joinColumns = @JoinColumn(name = "id_building"),
            inverseJoinColumns = @JoinColumn(name = "id_value"))
    private Set<Value> properties = new LinkedHashSet<>();

    public Set<Value> getProperties() {
        return properties;
    }

    public void setProperties(Set<Value> properties) {
        this.properties = properties;
    }

    public Set<BuildingLink> getLandingLinks() {
        return landingLinks;
    }

    public void setLandingLinks(Set<BuildingLink> landingLinks) {
        this.landingLinks = landingLinks;
    }

    public Set<BuildingLink> getDepartingLinks() {
        return departingLinks;
    }

    public void setDepartingLinks(Set<BuildingLink> departingLinks) {
        this.departingLinks = departingLinks;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Double getYCoordinate() {
        return yCoordinate;
    }

    public void setYCoordinate(Double yCoordinate) {
        this.yCoordinate = yCoordinate;
    }

    public Double getXCoordinate() {
        return xCoordinate;
    }

    public void setXCoordinate(Double xCoordinate) {
        this.xCoordinate = xCoordinate;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public BuildingValue getBuildingValue() {
        return buildingValue;
    }

    public void setBuildingValue(BuildingValue value) {
        this.buildingValue = value;
    }

    public District getDistrict() {
        return district;
    }

    public void setDistrict(District district) {
        this.district = district;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Value getKey() {
        return key;
    }

    public void setKey(Value key) {
        this.key = key;
    }
}