package com.project.cartosi.depth;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DepthRepository extends JpaRepository<Depth, Long> {
    @Query("SELECT dd FROM Depth dd WHERE dd.configuration.id=:configurationId")
    List<Depth> getDepthByConfiguration(Long configurationId);
}