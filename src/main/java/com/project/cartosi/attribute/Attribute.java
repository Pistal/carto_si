package com.project.cartosi.attribute;

import com.project.cartosi.configuration.Configuration;
import com.project.cartosi.depth.Depth;
import com.project.cartosi.link_type.LinkType;
import com.project.cartosi.value.Value;

import javax.persistence.*;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "attribute")
public class Attribute {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "id_configuration", nullable = false)
    private Configuration configuration;

    @Column(name = "name", nullable = false, length = 50)
    private String name;

    @OneToMany(mappedBy = "attribute", cascade = CascadeType.REMOVE, orphanRemoval = true)
    private Set<Value> values = new LinkedHashSet<>();

    @OneToOne(mappedBy = "attribute", cascade = CascadeType.REMOVE, orphanRemoval = true)
    private Depth depth;

    @OneToMany(mappedBy = "departingBuildingAttribute", cascade = CascadeType.REMOVE, orphanRemoval = true)
    private Set<LinkType> departingLinks;

    @OneToMany(mappedBy = "landingBuildingAttribute", cascade = CascadeType.REMOVE, orphanRemoval = true)
    private Set<LinkType> landingLinks;


    public Depth getDepth() {
        return depth;
    }

    public void setDepth(Set<Depth> depths) {
        this.depth = depth;
    }

    public Set<Value> getValues() {
        return values;
    }

    public void setValues(Set<Value> values) {
        this.values = values;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Configuration getConfiguration() {
        return configuration;
    }

    public void setConfiguration(Configuration configuration) {
        this.configuration = configuration;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Attribute attribute = (Attribute) o;
        return Objects.equals(getId(), attribute.getId()) && Objects.equals(getConfiguration(), attribute.getConfiguration()) && Objects.equals(getName(), attribute.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getConfiguration(), getName());
    }
}