package com.project.cartosi.attribute;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface AttributeRepository extends JpaRepository<Attribute, Long> {
    @Query("SELECT a FROM Attribute a WHERE a.configuration.id=:configId AND a.name=:name")
    Attribute getByNameAndConfigurationId(String name, Long configId);
}