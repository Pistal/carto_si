package com.project.cartosi.configuration;

import com.project.cartosi.app_user.AppUser;
import com.project.cartosi.attribute.Attribute;
import com.project.cartosi.depth.Depth;
import com.project.cartosi.header.Header;
import com.project.cartosi.link_type.LinkType;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.LinkedHashSet;
import java.util.Set;

@Entity
@Table(name = "configuration")
public class Configuration {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "name", nullable = false, length = 50)
    private String name;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "id_owner", nullable = false)
    private AppUser owner;

    @Column(name = "building_attribute", nullable = false, length = 50 )
    private String buildingAttribute;

    @Column(name = "building_color_attribute", nullable = false, length = 50)
    private String buildingColorAttribute;

    @OneToMany(mappedBy = "configuration", cascade = CascadeType.REMOVE, orphanRemoval = true)
    private Set<Attribute> attributes = new LinkedHashSet<>();

    @OneToOne(mappedBy = "configuration", cascade = CascadeType.REMOVE, orphanRemoval = true)
    private Header header;

    @OneToMany(mappedBy = "configuration", cascade = CascadeType.REMOVE, orphanRemoval = true)
    private Set<LinkType> linkTypes = new LinkedHashSet<>();

    @OneToMany(mappedBy = "configuration", cascade = CascadeType.REMOVE, orphanRemoval = true)
    private Set<Depth> depths = new LinkedHashSet<>();

    public Set<Depth> getDepths() {
        return depths;
    }

    public void setDepths(Set<Depth> depths) {
        this.depths = depths;
    }

    public Set<LinkType> getLinkTypes() {
        return linkTypes;
    }

    public void setLinkTypes(Set<LinkType> linkTypes) {
        this.linkTypes = linkTypes;
    }

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public Set<Attribute> getAttributes() {
        return attributes;
    }

    public void setAttributes(Set<Attribute> attributes) {
        this.attributes = attributes;
    }

    public String getBuildingColorAttribute() {
        return buildingColorAttribute;
    }

    public void setBuildingColorAttribute(String buildingColorAttribute) {
        this.buildingColorAttribute = buildingColorAttribute;
    }

    public String getBuildingAttribute() {
        return buildingAttribute;
    }

    public void setBuildingAttribute(String buildingAttribute) {
        this.buildingAttribute = buildingAttribute;
    }

    public AppUser getOwner() {
        return owner;
    }

    public void setOwner(AppUser owner) {
        this.owner = owner;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}