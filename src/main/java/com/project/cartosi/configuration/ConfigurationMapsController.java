package com.project.cartosi.configuration;

import com.project.cartosi.api.ConfigurationApi;
import com.project.cartosi.header.HeaderRepository;
import com.project.cartosi.pojos.AttributeValuePOJO;
import com.project.cartosi.pojos.ConfigurationProfilePOJO;
import com.project.cartosi.pojos.CreateMapPOJO;
import com.project.cartosi.pojos.NameValidityPOJO;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.*;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@RestController
public class ConfigurationMapsController implements ConfigurationApi {
    private final HeaderRepository headerRepository;
    private final MapBuilder mapBuilder;

    public ConfigurationMapsController(HeaderRepository headerRepository, MapBuilder mapBuilder) {
        this.headerRepository = headerRepository;
        this.mapBuilder = mapBuilder;
    }


    private static Path getXlsxStorageDirectoryPath(String mapName) throws IOException {
        try {
            return Files.createDirectories(Paths.get(System.getenv("CARTOSI_XLSX_STORAGE"), mapName));
        } catch (FileAlreadyExistsException e) {
            return Paths.get(System.getenv("CARTOSI_XLSX_STORAGE"), mapName);
        }
    }

    @Override
    public ResponseEntity<ConfigurationProfilePOJO> getProfile(Long configurationId) {
        return null;
    }


    @Override
    public ResponseEntity<List<AttributeValuePOJO>> submitExcel(String mapName, MultipartFile file) {
        DataFormatter dataFormatter = new DataFormatter();
        try(var inputStream = file.getInputStream()) {
            var workbook = WorkbookFactory.create(inputStream);
            var sheet = workbook.getSheetAt(2);

            if(sheet.getLastRowNum() < 3) {
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }

            var rowIterator = sheet.rowIterator();
            var attributes = new HashMap<String, Set<String>>();

            var headerRow = rowIterator.next();
            var inputFileHeaders = IntStream
                    .range(0, headerRow.getLastCellNum())
                    .mapToObj(i -> new AbstractMap.SimpleEntry<>(i, headerRow.getCell(i).getStringCellValue()))
                    .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
            inputFileHeaders.values().forEach(attr -> attributes.put(attr, new HashSet<>()));

            rowIterator.forEachRemaining(row -> {
                row.cellIterator().forEachRemaining(cell -> {
                    var value = dataFormatter.formatCellValue(cell);
                    var attribute = inputFileHeaders.get(cell.getColumnIndex());
                    var values = attributes.get(attribute);
                    values.add(value);
                });
            });

            var result = attributes.entrySet().stream().map(e -> {
                var av = new AttributeValuePOJO();
                av.setAttribute(e.getKey());
                av.setValues(e.getValue().stream().toList());
                return av;
            }).collect(Collectors.toList());

            var directoryPath = getXlsxStorageDirectoryPath(mapName);
            Files.copy(file.getInputStream(), directoryPath.resolve("input_data.xlsx"), StandardCopyOption.REPLACE_EXISTING);

            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (IOException e) {
            System.out.println(e);
            System.out.println(e.getMessage());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public ResponseEntity<NameValidityPOJO> verifyName(String mapName) {
        var count = headerRepository.countHeaderWithName(mapName);
        var nameValidityPojo = new NameValidityPOJO();
        nameValidityPojo.setValid(count == 0);
        return new ResponseEntity<>(nameValidityPojo, HttpStatus.OK);
    }



    @Override
    public ResponseEntity<Void> createMap(CreateMapPOJO createMapPOJO) {
        var result = mapBuilder.build(createMapPOJO);
        if(result) {
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }
}
