package com.project.cartosi.configuration;
import com.project.cartosi.pojos.CreateMapPOJO;
import org.springframework.stereotype.Component;

@Component
public interface MapBuilderService {
    boolean build(CreateMapPOJO createMapPOJO);
}
