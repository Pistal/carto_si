package com.project.cartosi.configuration;

import com.project.cartosi.app_user.AppUserRepository;
import com.project.cartosi.attribute.Attribute;
import com.project.cartosi.attribute.AttributeRepository;
import com.project.cartosi.building.Building;
import com.project.cartosi.building.BuildingRepository;
import com.project.cartosi.building_value.BuildingValue;
import com.project.cartosi.building_value.BuildingValueRepository;
import com.project.cartosi.depth.Depth;
import com.project.cartosi.depth.DepthRepository;
import com.project.cartosi.district.District;
import com.project.cartosi.district.DistrictRepository;
import com.project.cartosi.district_value.DistrictValue;
import com.project.cartosi.district_value.DistrictValueRepository;
import com.project.cartosi.header.Header;
import com.project.cartosi.header.HeaderRepository;
import com.project.cartosi.keycloak.KeycloakManager;
import com.project.cartosi.link.BuildingLink;
import com.project.cartosi.link.BuildingLinkRepository;
import com.project.cartosi.link_type.LinkType;
import com.project.cartosi.link_type.LinkTypeRepository;
import com.project.cartosi.permission.Permissions;
import com.project.cartosi.permission.UserPermission;
import com.project.cartosi.permission.UserPermissionRepository;
import com.project.cartosi.pojos.CreateMapPOJO;
import com.project.cartosi.utils.Tools;
import com.project.cartosi.value.Value;
import com.project.cartosi.value.ValueRepository;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Service("mapBuilder")
public class MapBuilder implements MapBuilderService {
    private final HeaderRepository headerRepository;
    private final AppUserRepository appUserRepository;
    private final ConfigurationRepository configurationRepository;
    private final UserPermissionRepository userPermissionRepository;
    private final BuildingValueRepository buildingValueRepository;
    private final LinkTypeRepository linkTypeRepository;
    private final DepthRepository depthRepository;
    private final DistrictValueRepository districtValueRepository;
    private final DistrictRepository districtRepository;
    private final BuildingRepository buildingRepository;
    private final BuildingLinkRepository buildingLinkRepository;
    private final AttributeRepository attributeRepository;
    private final ValueRepository valueRepository;
    private final KeycloakManager keycloakManager;

    private final List<Map<String, String>> excelData = new ArrayList<>();
    private final Map<Depth, Map<String, DistrictValue>> districtValuesByDepthAndValue = new HashMap<>();
    private final Map<String, Attribute> attributeByName = new HashMap<>();
    private final Map<String, BuildingValue> buildingValuesByValue = new HashMap<>();
    private final HashMap<Integer, Building> buildingForIndex = new HashMap<>();
    private final HashMap<LinkType, HashMap<String, Building>> buildingByLinkTypeAndKey = new HashMap<>();

    private CreateMapPOJO createMapPOJO;
    private Configuration configuration = new Configuration();
    private Header header = new Header();
    private LinkedList<Depth> depths = new LinkedList<>();
    private Set<Attribute> attributes = new HashSet<>();
    private Set<DistrictValue> districtValues = new HashSet<>();
    private Set<Value> values = new HashSet<>();
    private Set<District> districts = new HashSet<>();
    private Set<Building> buildings = new HashSet<>();
    private Set<BuildingValue> buildingValues = new HashSet<>();
    private Set<BuildingLink> buildingLinks = new HashSet<>();
    private Set<LinkType> linkTypes =  new HashSet<>();

    public MapBuilder(HttpServletRequest request, HeaderRepository headerRepository, AppUserRepository appUserRepository, ConfigurationRepository configurationRepository, UserPermissionRepository userPermissionRepository, BuildingValueRepository buildingValueRepository, LinkTypeRepository linkTypeRepository, DepthRepository depthRepository, DistrictValueRepository districtValueRepository, DistrictRepository districtRepository, BuildingRepository buildingRepository, BuildingLinkRepository buildingLinkRepository, AttributeRepository attributeRepository, ValueRepository valueRepository) {
        this.headerRepository = headerRepository;
        this.appUserRepository = appUserRepository;
        this.configurationRepository = configurationRepository;
        this.userPermissionRepository = userPermissionRepository;
        this.buildingValueRepository = buildingValueRepository;
        this.linkTypeRepository = linkTypeRepository;
        this.depthRepository = depthRepository;
        this.districtValueRepository = districtValueRepository;
        this.districtRepository = districtRepository;
        this.buildingRepository = buildingRepository;
        this.buildingLinkRepository = buildingLinkRepository;
        this.attributeRepository = attributeRepository;
        this.valueRepository = valueRepository;
        this.keycloakManager = new KeycloakManager(request);
    }

    private void parseExcel() throws IOException {
        var inputData = Path.of(System.getenv("CARTOSI_XLSX_STORAGE"), createMapPOJO.getName(), "input_data.xlsx").toFile();

        var inputStream = new FileInputStream(inputData);
        var workbook = WorkbookFactory.create(inputStream);
        var sheet = workbook.getSheetAt(2);

        var rowIterator = sheet.rowIterator();
        var headerRow = rowIterator.next();
        rowIterator.forEachRemaining(row -> {
            if(row.getLastCellNum() < 0) return;

            var data = new HashMap<String, String>();
            for (int i = 0; i < headerRow.getLastCellNum(); i++) {
                var cell = row.getCell(i);
                var attribute = headerRow.getCell(i).getStringCellValue();
                String value;
                if(cell == null) {
                    value = "";
                } else {
                    value = cell.getStringCellValue();
                }
                data.put(attribute, value);
            }
            excelData.add(data);
        });
    }

    private void init() throws IllegalAccessException {
        var optionalId = appUserRepository.findBySsoId(keycloakManager.getUserId());
        if(optionalId.isEmpty()){
            throw new IllegalAccessException();
        }
        var owner = appUserRepository.getById(optionalId.get());
        configuration.setAttributes(attributes);
        configuration.setOwner(owner);
        configuration.setDepths(Set.copyOf(depths));
        configuration.setLinkTypes(linkTypes);
        configuration.setName(createMapPOJO.getName() + " - configuration");
        configuration.setBuildingAttribute(createMapPOJO.getBuildingAttribute().getAttribute());
        configuration.setBuildingColorAttribute(createMapPOJO.getBuildingColorAttribute().getAttribute());
        configuration = configurationRepository.save(configuration);

        var date = LocalDate.now();
        header.setName(createMapPOJO.getName());
        header.setLastUser(owner);
        header.setCreationDate(date);
        header.setLastUpdate(date);
        header.setConfiguration(configuration);
        header = headerRepository.save(header);

        var userPermission = new UserPermission();
        userPermission.setUser(owner);
        userPermission.setPermission(Permissions.OWNER);
        userPermission.setHeader(header);
        userPermission = userPermissionRepository.save(userPermission);
    }

    private void buildAttribute() {
        excelData.get(0).forEach((key, value) -> {
            var attribute = new Attribute();
            attribute.setConfiguration(configuration);
            attribute.setName(key);
            attribute.setValues(new HashSet<>());
            attributes.add(attribute);
            attributeByName.put(key, attribute);
        });

        attributes = Set.copyOf(attributeRepository.saveAll(attributes));
    }

    private void buildLinkTypes() {
        createMapPOJO.getLinks().forEach(l -> {
            var linkType = new LinkType();
            linkType.setConfiguration(configuration);
            linkType.setColor(Tools.colorToString(l.getColor()));
            linkType.setThickness(l.getThickness());
            linkType.setLineType(l.getLineType());
            linkType.setDepartingBuildingAttribute(attributeByName.get(l.getFirstAttribute()));
            linkType.setLandingBuildingAttribute(attributeByName.get(l.getSecondAttribute()));
            linkTypes.add(linkType);
            buildingByLinkTypeAndKey.put(linkType, new HashMap<>());
        });

        linkTypes = Set.copyOf(linkTypeRepository.saveAll(linkTypes));
    }

    private void buildDepths() {
        IntStream.range(0, createMapPOJO.getDistrictAttributes().size()).forEach(i -> {
            var da = createMapPOJO.getDistrictAttributes().get(i);
            var depth = new Depth();
            depth.setConfiguration(configuration);
            depth.setAttribute(attributeByName.get(da.getAttribute()));
            depth.setDepth(i);
            depths.add(depth);

            var districtValueByValue = new HashMap<String, DistrictValue>();
            for(var v : da.getValues()) {
                if(v.getName().equals("")) continue;

                var districtValue = new DistrictValue();
                districtValue.setColor(Tools.colorToString(v.getColor()));
                districtValue.setSize(2);
                districtValue.setIcon("");
                districtValueByValue.put(Tools.trimString(v.getName()), districtValue);
            }
            districtValuesByDepthAndValue.put(depth, districtValueByValue);
        });

        depths.forEach(depth -> depth.setId(depthRepository.save(depth).getId()));
    }

    private void buildBuildingValues() {
        createMapPOJO.getBuildingColorAttribute().getValues().forEach(bca -> {
            var bv = new BuildingValue();
            bv.setColor(Tools.colorToString(bca.getColor()));
            bv.setSize(1);
            bv.setIcon("");
            buildingValuesByValue.put(bca.getName(), bv);
            buildingValues.add(bv);
        });
    }

    private void buildBuildingAndValues() {

        IntStream.range(0, excelData.size()).forEach(i -> {
            var row = excelData.get(i);
            var building = new Building();
            building.setXCoordinate(Math.random() * 2000);
            building.setYCoordinate(Math.random() * 2000);

            var rowValues = row.entrySet().stream().map(entry -> {
                var attributeName = entry.getKey();
                var name = Tools.trimString(entry.getValue());

                if(attributeName.equals(createMapPOJO.getBuildingColorAttribute().getAttribute())) {
                    var buildingValue = buildingValuesByValue.get(entry.getValue());
                    buildingValue.getBuildings().add(building);
                    building.setBuildingValue(buildingValue);
                }

                linkTypes.forEach(lt -> {
                    if(lt.getDepartingBuildingAttribute().getName().equals(attributeName)) {
                        var buildingByKey = buildingByLinkTypeAndKey.get(lt);
                        buildingByKey.put(name, building);
                    }
                });

                var attribute = attributeByName.get(attributeName);
                var optionalValue = attribute.getValues().stream().filter(v -> v.getName().equals(name)).findFirst();
                if(optionalValue.isEmpty()) {
                    var value = new Value();
                    value.setName(name);
                    value.setAttribute(attribute);
                    value = valueRepository.save(value);
                    attribute.getValues().add(value);

                    if(attributeName.equals(createMapPOJO.getBuildingAttribute().getAttribute())) {
                        building.setKey(value);
                    }

                    return value;
                } else {
                    if(attributeName.equals(createMapPOJO.getBuildingAttribute().getAttribute())) {
                        building.setKey(optionalValue.get());
                    }
                    return optionalValue.get();
                }
            }).collect(Collectors.toSet());

            building.setProperties(rowValues);

            buildingForIndex.put(i, building);
            buildings.add(building);
            values.addAll(rowValues);
        });

        values.forEach(v -> v.setId(valueRepository.save(v).getId()));
    }

    private void buildLinks() {
        linkTypes.forEach(lt -> IntStream.range(0, excelData.size()).forEach(i -> {
            var row = excelData.get(i);
            var firstKey = row.get(lt.getDepartingBuildingAttribute().getName());
            var secondKey = row.get(lt.getLandingBuildingAttribute().getName());

            if(firstKey.equals("") || secondKey.equals("")) return;
            if(firstKey.equals(secondKey)) return;

            var isFirstKeyReal = lt.getLandingBuildingAttribute()
                    .getValues()
                    .stream()
                    .map(Value::getName).collect(Collectors.toSet())
                    .contains(firstKey);
            var isSecondKeyReal = lt.getDepartingBuildingAttribute()
                    .getValues()
                    .stream()
                    .map(Value::getName).collect(Collectors.toSet())
                    .contains(secondKey);

            if(!isFirstKeyReal && !isSecondKeyReal) return;

            var link = new BuildingLink();
            link.setLinkType(lt);
            var buildingByKey = buildingByLinkTypeAndKey.get(lt);

            var departingBuilding = buildingByKey.get(firstKey);
            var landingBuilding = buildingByKey.get(secondKey);

            if(departingBuilding == null || landingBuilding == null) return;

            link.setDepartingBuilding(departingBuilding);
            link.setLandingBuilding(landingBuilding);
            buildingLinks.add(link);
        }));
        buildingLinks = Set.copyOf(buildingLinkRepository.saveAll(buildingLinks));
    }

    private void buildDistricts() {
        List<District> parentDistricts = new ArrayList<>();
        parentDistricts.add(null);
        for (Depth d : depths) {
            List<District> newParentDistricts = new ArrayList<>();
            for (Value v : d.getAttribute().getValues()) {
                if(v.getName().equals("")) continue;

                var districtValue = districtValuesByDepthAndValue.get(d).get(v.getName());
                districtValue.setValue(v);
                districtValue = districtValueRepository.save(districtValue);
                for (District parent : parentDistricts) {
                    var district = new District();
                    district.setDistrictValue(districtValue);
                    district.setParent(parent);
                    district.setXCoordinate(Math.random() * 2000);
                    district.setYCoordinate(Math.random() * 2000);
                    district = districtRepository.save(district);
                    newParentDistricts.add(district);
                    districtValue.getDistricts().add(district);
                }
            }
            districts.addAll(newParentDistricts);
            parentDistricts = newParentDistricts;
        }
    }

    private void updateBuildingValues() {
        var buildingColorAttribute = attributeByName.get(createMapPOJO.getBuildingColorAttribute().getAttribute());
        buildingColorAttribute.getValues().forEach(v -> {
            var buildingValue = buildingValuesByValue.get(v.getName());
            buildingValue.setValue(v);
        });
        buildingValues.forEach(buildingValue -> buildingValue.setId(buildingValueRepository.save(buildingValue).getId()));
    }

    private District findDistrictForBuilding(District district, Iterator<Depth> depthIterator, Map<String, String> buildingProperties) {
        if(!depthIterator.hasNext()) return district;
        var depth = depthIterator.next();
        var value = buildingProperties.get(depth.getAttribute().getName());
        if(value.equals("")) return district;
        var districtValue = districtValuesByDepthAndValue.get(depth).get(value);
        var newDistrict = districtValue.getDistricts().stream().filter(d -> Objects.equals(d.getParent(), district)).findFirst().orElseThrow();
        return findDistrictForBuilding(newDistrict, depthIterator, buildingProperties);
    }

    private void updateBuildings() {
        buildingForIndex.forEach((index, building) -> {
            var district = findDistrictForBuilding(null, depths.iterator(), excelData.get(index));
            building.setDistrict(district);
        });
        buildings = Set.copyOf(buildingRepository.saveAll(buildings));
    }

    private void buildAll() {
        buildAttribute();
        buildLinkTypes();
        buildDepths();
        buildBuildingValues();
        buildBuildingAndValues();
        buildDistricts();
        updateBuildingValues();
        updateBuildings();
        buildLinks();
    }

    private void reset() {
        excelData.clear();
        districtValuesByDepthAndValue.clear();
        attributeByName.clear();
        buildingValuesByValue.clear();
        buildingForIndex.clear();
        buildingByLinkTypeAndKey.clear();

        createMapPOJO = null;
        configuration = new Configuration();
        header = new Header();
        depths = new LinkedList<>();
        attributes = new HashSet<>();
        districtValues = new HashSet<>();
        values = new HashSet<>();
        districts = new HashSet<>();
        buildings = new HashSet<>();
        buildingValues = new HashSet<>();
        buildingLinks = new HashSet<>();
        linkTypes =  new HashSet<>();
    }

    @Override
    @Transactional
    public boolean build(CreateMapPOJO createMapPOJO) {
        this.createMapPOJO = createMapPOJO;
        try {
            parseExcel();
            init();
            buildAll();
            return true;
        } catch (IllegalAccessException | IOException e) {
            return false;
        } finally {
            reset();
        }
    }
}
