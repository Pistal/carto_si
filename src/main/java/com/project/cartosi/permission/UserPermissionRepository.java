package com.project.cartosi.permission;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserPermissionRepository extends JpaRepository<UserPermission, Long> {

    @Query("SELECT p FROM UserPermission p WHERE p.user.id = :userId")
    List<UserPermission> findPermissionsByUserId(Long userId);

    @Query("SELECT p FROM UserPermission p WHERE p.user.id = :userId AND p.header.id = :mapId")
    Optional<UserPermission> findPermissionByUserIdAndAndMapId(Long userId, Long mapId);
}