package com.project.cartosi.permission;

public enum Permissions {
    OWNER,
    EDITOR,
    VISITOR
}
