package com.project.cartosi.district;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DistrictRepository extends JpaRepository<District, Long> {
    @Query("SELECT d FROM District d WHERE d.districtValue.value.name=:value")
    List<District> getByValueName(String value);

    @Query("SELECT d FROM District d WHERE d.districtValue.value.attribute.configuration.id=:configurationId")
    List<District> getByConfiguration(Long configurationId);
}