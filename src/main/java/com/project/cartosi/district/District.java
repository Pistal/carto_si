package com.project.cartosi.district;

import com.project.cartosi.district_value.DistrictValue;
import com.project.cartosi.building.Building;

import javax.persistence.*;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "district")
public class District {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_parent")
    private District parent;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "id_district_value", nullable = false)
    private DistrictValue districtValue;

    @Column(name = "size")
    private Integer size;

    @Column(name = "x_coordinate", nullable = false)
    private Double xCoordinate;

    @Column(name = "y_coordinate", nullable = false)
    private Double yCoordinate;

    @Column(name = "comment", length = 50)
    private String comment;

    @Column(name = "color", length = 50)
    private String color;

    @Column(name = "icon", length = 50)
    private String icon;

    @OneToMany(mappedBy = "parent", cascade = CascadeType.REMOVE, orphanRemoval = true)
    private Set<District> districts = new LinkedHashSet<>();

    @OneToMany(mappedBy = "district", cascade = CascadeType.REMOVE, orphanRemoval = true)
    private Set<Building> buildings = new LinkedHashSet<>();

    public Set<Building> getBuildings() {
        return buildings;
    }

    public void setBuildings(Set<Building> buildings) {
        this.buildings = buildings;
    }

    public Set<District> getDistricts() {
        return districts;
    }

    public void setDistricts(Set<District> districts) {
        this.districts = districts;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Double getYCoordinate() {
        return yCoordinate;
    }

    public void setYCoordinate(Double yCoordinate) {
        this.yCoordinate = yCoordinate;
    }

    public Double getXCoordinate() {
        return xCoordinate;
    }

    public void setXCoordinate(Double xCoordinate) {
        this.xCoordinate = xCoordinate;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public DistrictValue getDistrictValue() {
        return districtValue;
    }

    public void setDistrictValue(DistrictValue districtValue) {
        this.districtValue = districtValue;
    }

    public District getParent() {
        return parent;
    }

    public void setParent(District parent) {
        this.parent = parent;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        District district = (District) o;
        return Objects.equals(getId(), district.getId()) && Objects.equals(getParent(), district.getParent()) && Objects.equals(getSize(), district.getSize()) && Objects.equals(xCoordinate, district.xCoordinate) && Objects.equals(yCoordinate, district.yCoordinate) && Objects.equals(getComment(), district.getComment()) && Objects.equals(getColor(), district.getColor()) && Objects.equals(getIcon(), district.getIcon());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getParent(), getSize(), xCoordinate, yCoordinate, getComment(), getColor(), getIcon());
    }
}