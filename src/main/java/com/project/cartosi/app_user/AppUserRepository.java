package com.project.cartosi.app_user;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AppUserRepository extends JpaRepository<AppUser, Long> {

    @Query("SELECT a.id FROM AppUser a WHERE a.idSso = :idSso")
    Optional<Long> findBySsoId(String idSso);

}