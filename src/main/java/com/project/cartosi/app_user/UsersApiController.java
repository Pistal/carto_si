package com.project.cartosi.app_user;

import com.project.cartosi.api.UsersApi;
import com.project.cartosi.pojos.UserPOJO;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class UsersApiController implements UsersApi {

    private final AppUserRepository appUserRepository;

    public UsersApiController(AppUserRepository appUserRepository) {
        this.appUserRepository = appUserRepository;
    }

    @Override
    public ResponseEntity<List<UserPOJO>> getAllUsers() {
        List<AppUser> result = appUserRepository.findAll();

        var pojos = new ArrayList<UserPOJO>();

        result.forEach(user -> {
            var pojo = new UserPOJO();
            pojo.setId(user.getId());
            pojo.setFirstName(user.getFirstname());
            pojo.setLastName(user.getLastname());
            pojos.add(pojo);
        });


        return new ResponseEntity<>(pojos, HttpStatus.OK);
    }
}
