package com.project.cartosi.app_user;

import com.project.cartosi.api.LogoutApi;
import com.project.cartosi.api.UserApi;
import com.project.cartosi.header.Header;
import com.project.cartosi.header.HeaderRepository;
import com.project.cartosi.keycloak.KeycloakManager;
import com.project.cartosi.permission.Permissions;
import com.project.cartosi.permission.UserPermission;
import com.project.cartosi.permission.UserPermissionRepository;
import com.project.cartosi.pojos.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import java.util.*;

@RestController
public class UserApiController implements UserApi, LogoutApi {

    private final KeycloakManager keycloakManager;
    private final AppUserRepository appUserRepository;
    private final HeaderRepository headerRepository;
    private final UserPermissionRepository userPermissionRepository;

    @Autowired
    public UserApiController(HttpServletRequest request, AppUserRepository appUserRepository, HeaderRepository headerRepository, UserPermissionRepository userPermissionRepository) {
        this.keycloakManager = new KeycloakManager(request);
        this.appUserRepository = appUserRepository;
        this.headerRepository = headerRepository;
        this.userPermissionRepository = userPermissionRepository;
    }

    @Override
    public ResponseEntity<Void> addPermissions(UserMapsBody body) {
        Objects.requireNonNull(body);
        Objects.requireNonNull(body.getPermission());
        Objects.requireNonNull(body.getUserId());
        Objects.requireNonNull(body.getMapId());
        if(body.getUserId() <= 0)
            throw new IllegalArgumentException("User ID is not a positive number");
        if(body.getMapId() <= 0)
            throw new IllegalArgumentException("Map ID is not a positive number");
        if(body.getPermission().isEmpty() || body.getPermission().isBlank())
            throw new IllegalArgumentException("String is blank");
        if(body.getPermission().isEmpty() || body.getPermission().isBlank())
            throw new IllegalArgumentException("String is blank");
        if(!body.getPermission().equals(Permissions.OWNER.name()) && !body.getPermission().equals(Permissions.VISITOR.name()) && !body.getPermission().equals(Permissions.EDITOR.name()))
            throw new IllegalArgumentException("Given permission does not exist");
        if(body.getPermission().equals(Permissions.OWNER.name()))
            throw new IllegalArgumentException("A map can not change owner");

        Optional<AppUser> optionalUser = appUserRepository.findById(body.getUserId());
        Optional<Header> optionalHeader = headerRepository.findById(body.getMapId());

        if(optionalUser.isEmpty() || optionalHeader.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        AppUser user = optionalUser.get();
        Header header = optionalHeader.get();

        var existingUserPermissionOptional = userPermissionRepository.findPermissionByUserIdAndAndMapId(body.getUserId(), body.getMapId());

        if(existingUserPermissionOptional.isPresent()){
            var existingUserPermission = existingUserPermissionOptional.get();
            if(existingUserPermission.getPermission().equals(Permissions.OWNER)){
                throw new IllegalStateException("Can't change owner permissions");
            }
            if(!existingUserPermission.getPermission().name().equals(body.getPermission())){
                existingUserPermission.setPermission(Permissions.valueOf(body.getPermission()));
                userPermissionRepository.saveAndFlush(existingUserPermission);
            }
        } else {
            UserPermission userPermission = new UserPermission();
            userPermission.setUser(user);
            userPermission.setHeader(header);
            userPermission.setPermission(Permissions.valueOf(body.getPermission()));
            userPermissionRepository.saveAndFlush(userPermission);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Void> deleteUserPermission(Long userId, Long mapId) {
        Objects.requireNonNull(userId);
        Objects.requireNonNull(mapId);
        if(userId <= 0)
            throw new IllegalArgumentException("User ID is not a positive number");
        if(mapId <= 0)
            throw new IllegalArgumentException("Map ID is not a positive number");

        var userPermissionOptional = userPermissionRepository.findPermissionByUserIdAndAndMapId(userId, mapId);
        if(userPermissionOptional.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        var userPermission = userPermissionOptional.get();

        if(userPermission.getPermission().equals(Permissions.OWNER)){
            throw new IllegalStateException("Can't delete owner");
        }

        userPermissionRepository.deleteById(userPermission.getId());

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Override
    public ResponseEntity<List<DroitUserPOJO>> getAllUserWithPermissionOnMap(Long mapId) {
        Objects.requireNonNull(mapId);
        if(mapId < 0)
            throw new IllegalArgumentException("Map ID is negative");

        var optionalId = appUserRepository.findBySsoId(keycloakManager.getUserId());
        if(optionalId.isEmpty()){
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
        var ownerId = optionalId.get();

        var optionalHeader = headerRepository.findById(mapId);
        if(optionalHeader.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        var header = optionalHeader.get();
        var permissionSet = header.getUserPermissions();

        var pojos = new ArrayList<DroitUserPOJO>();
        permissionSet.forEach(userPermission -> {
            // Do not add the owner in the response
            if(userPermission.getUser().getId().equals(ownerId)){
                return;
            }
            var pojo = new DroitUserPOJO();
            pojo.setId(userPermission.getUser().getId());
            pojo.setName(userPermission.getUser().getFirstname() + " " + userPermission.getUser().getLastname()); // TODO nom + prénom
            pojo.setPermission(userPermission.getPermission().name());
            pojos.add(pojo);
        });

        return new ResponseEntity<>(pojos, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<UserPOJO> getUser() {
        var userPojo = new UserPOJO();

        var idSso = keycloakManager.getUserId();
        var firstName = keycloakManager.getGivenName();
        var lastName = keycloakManager.getFamilyName();
        firstName = firstName == null ? "" : firstName;
        lastName = lastName == null ? "" : lastName;

        userPojo.setFirstName(firstName);
        userPojo.setLastName(lastName);

        var optionalId = appUserRepository.findBySsoId(idSso);
        if(optionalId.isEmpty()){
            var appUser = new AppUser();
            appUser.setFirstname(firstName);
            appUser.setLastname(lastName);
            appUser.setIdSso(idSso);
            var tmp = appUserRepository.saveAndFlush(appUser);
            userPojo.setId(tmp.getId());
        } else {
            userPojo.setId(optionalId.get());
        }
        return new ResponseEntity<>( userPojo, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Void> logout() {
        try {
            keycloakManager.logout();
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (ServletException e) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
    }
}
