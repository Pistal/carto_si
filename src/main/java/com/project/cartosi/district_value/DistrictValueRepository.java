package com.project.cartosi.district_value;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DistrictValueRepository extends JpaRepository<DistrictValue, Long> {
    @Query("SELECT dv FROM DistrictValue dv WHERE dv.value.attribute.configuration.id=:configurationId")
    List<DistrictValue> getByConfigurationId(Long configurationId);

    @Query("SELECT dv FROM DistrictValue dv WHERE dv.value.attribute.depth.id=:id")
    List<DistrictValue> getByDepthId(Long id);
}