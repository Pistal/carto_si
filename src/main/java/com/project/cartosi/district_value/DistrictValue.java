package com.project.cartosi.district_value;

import com.project.cartosi.district.District;
import com.project.cartosi.value.Value;

import javax.persistence.*;
import java.util.LinkedHashSet;
import java.util.Set;

@Entity
@Table(name = "district_value")
public class DistrictValue {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "size", nullable = false)
    private Integer size;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "id_value", nullable = false)
    private Value value;

    @Column(name = "color", nullable = false, length = 50)
    private String color;

    @Column(name = "icon", nullable = false, length = 50)
    private String icon;

    @OneToMany(mappedBy = "districtValue", cascade = CascadeType.REMOVE, orphanRemoval = true)
    private Set<District> districts = new LinkedHashSet<>();

    public Set<District> getDistricts() {
        return districts;
    }

    public void setDistricts(Set<District> districts) {
        this.districts = districts;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Value getValue() {
        return value;
    }

    public void setValue(Value value) {
        this.value = value;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}