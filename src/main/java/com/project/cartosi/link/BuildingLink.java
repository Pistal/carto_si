package com.project.cartosi.link;

import com.project.cartosi.building.Building;
import com.project.cartosi.link_type.LinkType;

import javax.persistence.*;

@Entity
@Table(name = "building_link")
public class BuildingLink {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "id_building_1", nullable = false)
    private Building departingBuilding;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "id_link_type", nullable = false)
    private LinkType linkType;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "id_building_2", nullable = false)
    private Building landingBuilding;

    @Column(name = "color", length = 50)
    private String color;

    @Column(name = "thickness")
    private Integer thickness;

    @Column(name = "line_type")
    private Integer lineType;

    public Integer getLineType() {
        return lineType;
    }

    public void setLineType(Integer lineType) {
        this.lineType = lineType;
    }

    public Integer getThickness() {
        return thickness;
    }

    public void setThickness(Integer thickness) {
        this.thickness = thickness;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Building getLandingBuilding() {
        return landingBuilding;
    }

    public void setLandingBuilding(Building landingBuilding) {
        this.landingBuilding = landingBuilding;
    }

    public LinkType getLinkType() {
        return linkType;
    }

    public void setLinkType(LinkType linkType) {
        this.linkType = linkType;
    }

    public Building getDepartingBuilding() {
        return departingBuilding;
    }

    public void setDepartingBuilding(Building departingBuilding) {
        this.departingBuilding = departingBuilding;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}