package com.project.cartosi.link;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BuildingLinkRepository extends JpaRepository<BuildingLink, Long> {
}