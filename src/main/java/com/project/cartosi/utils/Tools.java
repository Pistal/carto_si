package com.project.cartosi.utils;

import com.project.cartosi.pojos.ColorPOJO;

public abstract class Tools {
    public static String colorToString(ColorPOJO colorPOJO) {
        return String.format("#%02x%02x%02x%02x", colorPOJO.getR(), colorPOJO.getG(), colorPOJO.getB(), colorPOJO.getA());
    }

    public static String trimString(String value) {
        if(value.length() >= 255) {
            return value.substring(0, 255);
        }
        return value;
    }
}
