package com.project.cartosi.building_property;

import org.hibernate.Hibernate;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class BuildingPropertyId implements Serializable {
    private static final long serialVersionUID = -151930903994617317L;
    @Column(name = "id_value", nullable = false)
    private Long idValue;
    @Column(name = "id_building", nullable = false)
    private Long idBuilding;

    public Long getIdBuilding() {
        return idBuilding;
    }

    public void setIdBuilding(Long idBuilding) {
        this.idBuilding = idBuilding;
    }

    public Long getIdValue() {
        return idValue;
    }

    public void setIdValue(Long idValue) {
        this.idValue = idValue;
    }

    @Override
    public int hashCode() {
        return Objects.hash(idBuilding, idValue);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        BuildingPropertyId entity = (BuildingPropertyId) o;
        return Objects.equals(this.idBuilding, entity.idBuilding) &&
                Objects.equals(this.idValue, entity.idValue);
    }
}