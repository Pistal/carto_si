package com.project.cartosi.building_property;

import com.project.cartosi.building.Building;
import com.project.cartosi.value.Value;

import javax.persistence.*;

@Entity
@Table(name = "building_property")
public class BuildingProperty {
    @EmbeddedId
    private BuildingPropertyId id;

    @MapsId("idValue")
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "id_value", nullable = false)
    private Value value;

    @MapsId("idBuilding")
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "id_building", nullable = false)
    private Building building;

    public Building getBuilding() {
        return building;
    }

    public void setBuilding(Building idBuilding) {
        this.building = idBuilding;
    }

    public Value getValue() {
        return value;
    }

    public void setValue(Value idValue) {
        this.value = idValue;
    }

    public BuildingPropertyId getId() {
        return id;
    }

    public void setId(BuildingPropertyId id) {
        this.id = id;
    }
}