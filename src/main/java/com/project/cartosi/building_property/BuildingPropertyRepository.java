package com.project.cartosi.building_property;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;


public interface BuildingPropertyRepository extends JpaRepository<BuildingProperty, BuildingPropertyId> {
    @Transactional
    @Modifying
    @Query("DELETE FROM BuildingProperty bp WHERE bp.value.id in (SELECT v.id FROM Value v WHERE v.attribute.configuration.header.id=:mapId)")
    void deleteByMapId(Long mapId);
}