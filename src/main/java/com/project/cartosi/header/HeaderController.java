package com.project.cartosi.header;

import com.project.cartosi.api.MapsApi;
import com.project.cartosi.app_user.AppUserRepository;
import com.project.cartosi.attribute.Attribute;
import com.project.cartosi.building.Building;
import com.project.cartosi.building.BuildingRepository;
import com.project.cartosi.building_property.BuildingPropertyRepository;
import com.project.cartosi.configuration.ConfigurationRepository;
import com.project.cartosi.district.DistrictRepository;
import com.project.cartosi.keycloak.KeycloakManager;
import com.project.cartosi.permission.Permissions;
import com.project.cartosi.permission.UserPermissionRepository;
import com.project.cartosi.pojos.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
public class HeaderController implements MapsApi {

    private final KeycloakManager keycloakManager;
    private final AppUserRepository appUserRepository;
    private final HeaderRepository headerRepository;
    private final ConfigurationRepository configurationRepository;
    private final UserPermissionRepository userPermissionRepository;
    private final BuildingRepository buildingRepository;
    private final DistrictRepository districtRepository;
    private final BuildingPropertyRepository buildingPropertyRepository;

    @Autowired
    public HeaderController(HttpServletRequest request, AppUserRepository appUserRepository, ConfigurationRepository configurationRepository, UserPermissionRepository userPermissionRepository, HeaderRepository headerRepository, BuildingRepository buildingRepository, DistrictRepository districtRepository, BuildingPropertyRepository buildingPropertyRepository) {
        this.keycloakManager = new KeycloakManager(request);
        this.appUserRepository = appUserRepository;

        this.configurationRepository = configurationRepository;
        this.userPermissionRepository = userPermissionRepository;
        this.headerRepository = headerRepository;
        this.buildingRepository = buildingRepository;
        this.districtRepository = districtRepository;
        this.buildingPropertyRepository = buildingPropertyRepository;
    }

    //@Override
    public ResponseEntity<Void> createMap(ConfigurationProfilePOJO body) {
        return null;
    }

    @Override
    public ResponseEntity<MapCanvasPOJO> getMap(Long mapId) {
        // Retrieve userId
        var optionalId = appUserRepository.findBySsoId(keycloakManager.getUserId());
        if(optionalId.isEmpty()){
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
        var userId = optionalId.get();

        // Check if user has permission on the map
        var optionalPermission = userPermissionRepository.findPermissionByUserIdAndAndMapId(userId, mapId);
        if(optionalPermission.isEmpty()){
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }

        // Retrieve map
        var optionalHeader = headerRepository.findById(mapId);
        if( optionalHeader.isEmpty() ){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        var header = optionalHeader.get();
        var config = header.getConfiguration();

        var districts = new ArrayList<DistrictPOJO>();
        var nodes     = new ArrayList<BuildingPOJO>();
        var edges     = new ArrayList<LinkPOJO>();
        var filters   = new ArrayList<FilterPOJO>();


        districtRepository.getByConfiguration(config.getId()).forEach(d -> {
            var districtPOJO = new DistrictPOJO();
            districtPOJO.setId(d.getId());
            districtPOJO.setLabel(d.getDistrictValue().getValue().getName());
            districtPOJO.setColor(d.getColor() != null ? d.getColor() : d.getDistrictValue().getColor());
            districtPOJO.setSize(d.getSize() != null ? d.getSize() : d.getDistrictValue().getSize());
            districtPOJO.setX(d.getXCoordinate());
            districtPOJO.setY(d.getYCoordinate());
            districtPOJO.setIdDistrictValue(d.getDistrictValue().getId());
            districtPOJO.setIdParent(d.getParent() != null ? d.getParent().getId() : null);
            districts.add(districtPOJO);
        });


        Set<Attribute> attributes = headerRepository.getById(mapId).getConfiguration().getAttributes();
        attributes.forEach(attribute -> {
            FilterPOJO filterPOJO = new FilterPOJO();
            filterPOJO.setName(attribute.getName());
            ArrayList<String> listValues = new ArrayList<>();
            attribute.getValues().forEach(value -> {
                listValues.add(value.getName());
            });
            filterPOJO.setAttributes(listValues);
            filters.add(filterPOJO);
        });

        List<Building> buildingList = buildingRepository.getBuildingByConfiguration(config.getId());
        buildingList.forEach(building -> {

            BuildingPOJO pojo = new BuildingPOJO();

            pojo.setId(building.getId());
            pojo.setIdDistrict(building.getDistrict() != null ? building.getDistrict().getId() : null);

            pojo.setLabel(building.getKey().getName());
            pojo.setSize(building.getSize() != null ? building.getSize() : building.getBuildingValue().getSize());
            pojo.setColor(building.getColor() != null ? building.getColor() : building.getBuildingValue().getColor());

            pojo.setX(building.getXCoordinate());
            pojo.setY(building.getYCoordinate());

            ArrayList<AttributePOJO> attributePOJOS = new ArrayList<>();
            building.getProperties().forEach(value -> {
                var attribute1 = new AttributePOJO();
                attribute1.setAttributeValue(value.getName());
                attribute1.setAttributeKey(value.getAttribute().getName());
                attributePOJOS.add(attribute1);
            });

            pojo.setAttributes(attributePOJOS);
            nodes.add(pojo);
        });

        config.getLinkTypes().forEach(linkType -> {
            linkType.getBuildingLinks().forEach(link -> {
                var pojo = new LinkPOJO();
                pojo.setIdBuilding1(link.getDepartingBuilding().getId());
                pojo.setIdBuilding2(link.getLandingBuilding().getId());
                pojo.setColor(link.getColor() != null ? link.getColor() : linkType.getColor());
                pojo.setSize(link.getThickness() != null ? link.getThickness() : linkType.getThickness());
                edges.add(pojo);
            });
        });

        var mapCanvasPojo = new MapCanvasPOJO();
        mapCanvasPojo.setHeader(header.getName());
        mapCanvasPojo.setBuildings(nodes);
        mapCanvasPojo.setLinks(edges);
        mapCanvasPojo.setDistricts(districts);
        mapCanvasPojo.setFilters(filters);

        return new ResponseEntity<>( mapCanvasPojo, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<List<MapUserPOJO>> getMaps() {
        // Retrieve userId
        var optionalId = appUserRepository.findBySsoId(keycloakManager.getUserId());
        if(optionalId.isEmpty()){
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
        var permissionEntities = userPermissionRepository.findPermissionsByUserId(optionalId.get());

        if(permissionEntities.size() == 0){
            return new ResponseEntity<>(List.of(), HttpStatus.OK);
        }

        var pojos = permissionEntities.stream()
                .map(permission -> {
                    var header = permission.getHeader();
                    var pojo = new MapUserPOJO();
                    pojo.setId(header.getId());
                    pojo.setName(header.getName());
                    pojo.setLastUser(header.getLastUser().getFirstname() + " " + header.getLastUser().getLastname());
                    pojo.setCreationDate(header.getCreationDate().toString());
                    pojo.setPermission(permission.getPermission().toString());

                    return pojo;
                })
                .collect(Collectors.toList());

        return new ResponseEntity<>(pojos, HttpStatus.OK);
    }

    @Override
    @Transactional
    public ResponseEntity<Void> deleteMap(Long mapId) {
        // Retrieve userId
        var optionalId = appUserRepository.findBySsoId(keycloakManager.getUserId());
        if(optionalId.isEmpty()){
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
        var userId = optionalId.get();

        var optionalPermission = userPermissionRepository.findPermissionByUserIdAndAndMapId(userId, mapId);
        if(optionalPermission.isEmpty()){
            System.out.println("Didn't find permission");
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
        var permission = optionalPermission.get();

        // Delete only the permissionEntity between user and the map
        userPermissionRepository.deleteById(permission.getId());

        // If owner : delete the whole map with attributes and links to it ...
        if( permission.getPermission().equals(Permissions.OWNER) ) {
            buildingPropertyRepository.deleteByMapId(mapId);
            headerRepository.deleteById(mapId);

        }

        return new ResponseEntity<>(HttpStatus.OK);
    }

}