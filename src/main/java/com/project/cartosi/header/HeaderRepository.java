package com.project.cartosi.header;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface HeaderRepository extends JpaRepository<Header, Long> {
    @Query("SELECT COUNT(h.name) FROM Header h WHERE h.name = :name")
    Integer countHeaderWithName(String name);
}