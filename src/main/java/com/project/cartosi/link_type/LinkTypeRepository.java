package com.project.cartosi.link_type;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LinkTypeRepository extends JpaRepository<LinkType, Long> {
    @Query("SELECT lt FROM LinkType lt WHERE lt.configuration.id=:configurationId")
    List<LinkType> getLinkTypeByConfiguration(Long configurationId);
}