package com.project.cartosi.link_type;

import com.project.cartosi.attribute.Attribute;
import com.project.cartosi.configuration.Configuration;
import com.project.cartosi.link.BuildingLink;

import javax.persistence.*;
import java.util.LinkedHashSet;
import java.util.Set;

@Entity
@Table(name = "link_type")
public class LinkType {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "id_configuration", nullable = false)
    private Configuration configuration;

    @Column(name = "color", nullable = false, length = 50)
    private String color;

    @Column(name = "thickness", nullable = false)
    private Integer thickness;

    @Column(name = "line_type", nullable = false)
    private Integer lineType;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_attribute_1")
    private Attribute departingBuildingAttribute;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_attribute_2")
    private Attribute landingBuildingAttribute;

    @OneToMany(mappedBy = "linkType", cascade = CascadeType.REMOVE, orphanRemoval = true)
    private Set<BuildingLink> buildingLinks = new LinkedHashSet<>();

    public Set<BuildingLink> getBuildingLinks() {
        return buildingLinks;
    }

    public void setBuildingLinks(Set<BuildingLink> buildingLinks) {
        this.buildingLinks = buildingLinks;
    }

    public Attribute getLandingBuildingAttribute() {
        return landingBuildingAttribute;
    }

    public void setLandingBuildingAttribute(Attribute landingBuildingAttribute) {
        this.landingBuildingAttribute = landingBuildingAttribute;
    }

    public Attribute getDepartingBuildingAttribute() {
        return departingBuildingAttribute;
    }

    public void setDepartingBuildingAttribute(Attribute departingBuildingAttribute) {
        this.departingBuildingAttribute = departingBuildingAttribute;
    }

    public Integer getLineType() {
        return lineType;
    }

    public void setLineType(Integer lineType) {
        this.lineType = lineType;
    }

    public Integer getThickness() {
        return thickness;
    }

    public void setThickness(Integer thickness) {
        this.thickness = thickness;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Configuration getConfiguration() {
        return configuration;
    }

    public void setConfiguration(Configuration configuration) {
        this.configuration = configuration;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}