package com.project.cartosi.building_value;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface BuildingValueRepository extends JpaRepository<BuildingValue, Long> {
    @Query("SELECT bv FROM BuildingValue bv WHERE bv.value.name=:name AND bv.value.attribute.configuration.id=:configurationId")
    BuildingValue getByNameAndConfigurationId(String name, Long configurationId);
}