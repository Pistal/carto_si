package com.project.cartosi.keycloak;

import org.keycloak.KeycloakSecurityContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import java.util.Objects;

public class KeycloakManager {

    private final HttpServletRequest request;

    public KeycloakManager(HttpServletRequest request) {
        Objects.requireNonNull(request);
        this.request = request;
    }

    public String getID(){
        return getKeycloakSecurityContext().getIdToken().getId();
    }

    public String getUserId(){
        return getKeycloakSecurityContext().getToken().getSubject();
    }

    public String getGivenName(){
        return getKeycloakSecurityContext().getIdToken().getGivenName();
    }

    public String getFamilyName(){
        return getKeycloakSecurityContext().getIdToken().getFamilyName();
    }

    public void logout() throws ServletException {
        request.logout();
    }

    private KeycloakSecurityContext getKeycloakSecurityContext() {
        return (KeycloakSecurityContext) request.getAttribute(KeycloakSecurityContext.class.getName());
    }

}