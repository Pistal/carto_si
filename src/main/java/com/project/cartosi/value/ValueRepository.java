package com.project.cartosi.value;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ValueRepository extends JpaRepository<Value, Long> {
    @Query("SELECT v FROM Value v WHERE v.name=:name")
    Value getByName(String name);

    @Query("SELECT v FROM Value v WHERE v.attribute.id=:attributeId")
    List<Value> getByAttributeId(Long attributeId);

    @Query("SELECT v FROM Value v WHERE v.attribute.name=:attributeName AND v.name=:name AND v.attribute.configuration.id=:configurationId")
    Value getByAttributeNameAndNameAndConfigurationId(String attributeName, String name, Long configurationId);
}