package com.project.cartosi.value;

import com.project.cartosi.attribute.Attribute;
import com.project.cartosi.building.Building;
import com.project.cartosi.district_value.DistrictValue;
import com.project.cartosi.building_value.BuildingValue;

import javax.persistence.*;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "value")
public class Value {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_attribute")
    private Attribute attribute;

    @Column(name = "name", nullable = false)
    private String name;

    @OneToMany(mappedBy = "value", cascade = CascadeType.REMOVE, orphanRemoval = true)
    private Set<BuildingValue> buildingValues = new LinkedHashSet<>();

    @OneToMany(mappedBy = "value", cascade = CascadeType.REMOVE, orphanRemoval = true)
    private Set<DistrictValue> districtValues = new LinkedHashSet<>();

    @OneToOne(mappedBy = "key", cascade = CascadeType.REMOVE, orphanRemoval = true)
    private Building building;

    public Building getBuilding() {
        return building;
    }

    public void setBuilding(Building building) {
        this.building = building;
    }

    public Attribute getAttribute() {
        return attribute;
    }

    public void setAttribute(Attribute idAttribute) {
        this.attribute = idAttribute;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<DistrictValue> getDistrictValues() {
        return districtValues;
    }

    public void setDistrictValues(Set<DistrictValue> districtValues) {
        this.districtValues = districtValues;
    }

    public Set<BuildingValue> getBuildingValues() {
        return buildingValues;
    }

    public void setBuildingValues(Set<BuildingValue> buildingValues) {
        this.buildingValues = buildingValues;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Value value = (Value) o;
        return Objects.equals(getId(), value.getId()) && Objects.equals(getAttribute(), value.getAttribute()) && Objects.equals(getName(), value.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getAttribute(), getName());
    }
}