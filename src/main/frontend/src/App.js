import './App.css';
import Catalogue from './pages/catalogue/Catalogue';
import Canvas from './pages/canvas/Canvas';
import {Container} from 'react-bootstrap';
import logo from './assets/img/logo.png';

import {HashRouter as Router, Routes, Route, Link} from "react-router-dom";
import React, {useEffect, useState} from "react";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

export default function App() {

    const USER_FETCH_URL = '/user';
    const [title, setTitle] = useState("Carto Si");
    const [user, setUser] = useState({});
    const [userReady, setUserReady] = useState(false);

    useEffect( () => {
        console.log("FETCHING USER");
        const fetchUser = () => {
            fetch(USER_FETCH_URL)
                .then(response => response.json())
                .then(data => {
                    console.log(data);
                    setUser(data);
                    setUserReady(true);
                })
                .catch(function(error){
                    console.error("FETCH USER FAILED", error);
                });
        };

        fetchUser();
    }, []); // eslint-disable-line react-hooks/exhaustive-deps


    const logout = () => {
        fetch("/logout")
            .then(result => {
                window.location.replace("")
            })
    }


    /* ALL ROUTES  ARE HERE */
    const ALL_ROUTES =
        <Routes>
            <Route path="/" element={<Catalogue updateTitle={() => setTitle("Mes cartes")} />} />
            <Route path="/edit/:mapId" element={<Canvas updateTitle={() => setTitle("Cartographie")} />} />

        </Routes>;

  return (
      <Router>
          <div className="App">
              <header className="App-header">
                  <div className={"App-nav"} style={{maxHeight: "none"}}>
                      <div ><Link to={'/'}><img src={logo} alt="Logo" /></Link></div>
                      <div>{title}</div>
                      <div className={"d-flex justify-content-center align-items-center"}>
                          <div className="circle-singleline me-3">{userReady && (user.firstName.charAt(0)+user.lastName.charAt(0)).toUpperCase()}</div>
                          <FontAwesomeIcon id={"logout_button"} icon="fa-solid fa-power-off" onClick={logout}/>
                      </div>
                  </div>
              </header>
          </div>
          {ALL_ROUTES}
      </Router>
  );
}