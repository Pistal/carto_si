import { createSlice } from '@reduxjs/toolkit'
import {mapAttributeWithRandomColor} from "../tools/redux_formatter";

export const creationFormSlice = createSlice({
    name: "creationFormData",
    initialState: {
        isEmpty: true,
        name: null,
        attributes: null,
        districtAttributes: [null],
        buildingAttribute: null,
        buildingColorAttribute: null,
        links: []
    },
    reducers: {
        setName: (state, action) => {
          state.name = action.payload
        },
        setAttributes: (state, action) => {
            state.isEmpty = false
            state.attributes = action.payload
            state.districtAttributes = [state.attributes[0]].map(mapAttributeWithRandomColor)
            state.buildingAttribute = state.attributes[1]
            state.buildingColorAttribute = mapAttributeWithRandomColor(state.attributes[2])
        },
        removeAttributes: (state, action) => {
            action.payload.forEach(k => delete state.attributes[k])
            state.isEmpty = false
        },
        mergeAttributes: (state, action) => {
            action.payload.forEach(a => {
                const entry = state.attributes.find(attr => attr.attribute === a.attribute)
                if(entry === undefined) {
                    state.attributes.push(a)
                } else {
                    a.values.forEach(value => {
                        if(!(entry.values.includes(value))) {
                            entry.values.push(value)
                        }
                    })
                }
            })
            state.isEmpty = false;
        },
        clearAttributes: (state) => {
            state.isEmpty = true
            state.attributes = null
        },
        setDistrictAttributes: (state, action) => {
            state.districtAttributes = action.payload
        },
        setBuildingAttribute: (state, action) => {
            state.buildingAttribute = action.payload
        },
        setBuildingColorAttribute: (state, action) => {
            state.buildingColorAttribute = action.payload
        },
        setLinks: (state, action) => {
            state.links = action.payload
        }
    }
})

export const {
    setName,
    setAttributes,
    removeAttributes,
    mergeAttributes,
    clearAttributes,
    setDistrictAttributes,
    setBuildingAttribute,
    setBuildingColorAttribute,
    setLinks
} = creationFormSlice.actions

export const getData = (state) => state.creationFormData
export const getAttributes = (state) => state.creationFormData.attributes
export const getDistrictAttributes = (state) => state.creationFormData.districtAttributes
export const getBuildingAttribute = (state) => state.creationFormData.buildingAttribute
export const getBuildingColorAttribute = (state) => state.creationFormData.buildingColorAttribute
export const getName = (state) => state.creationFormData.name
export const getLinks = (state) => state.creationFormData.links
export default creationFormSlice.reducer