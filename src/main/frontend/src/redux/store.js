import { configureStore } from '@reduxjs/toolkit'
import creationFormReducer from './creationFormSlice'

export default configureStore({
    reducer: {
        creationFormData: creationFormReducer,
    },
})