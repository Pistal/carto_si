import './MiniatureMap.css';
import logo from '../../assets/img/logo.png';
import {Row, Col} from "react-bootstrap";
import Button from '@mui/material/Button';
import DialogTitle from '@mui/material/DialogTitle';
import Dialog from '@mui/material/Dialog';
import Droit from '../../pages/droit/Droit';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {forwardRef, useState} from "react";
import {Link} from "react-router-dom";
import {api_request_headers} from "../../tools/api_request_header";
import LoadingScreen from "../canvas_container/loading_screen/LoadingScreen";
import Snackbar from "@mui/material/Snackbar";
import MuiAlert from "@mui/material/Alert";


const Alert = forwardRef(function Alert(props, ref) {
    return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
});
// Typical carte object :
// props.map: {
//         'id' : 1,
//         'name' : "Archi SI",
//         'last_update' : "Nicolas Boutin",
//         'creation_date' : Date.parse("2022-02-05"),
//         'permission' : 'OWNER'
//},

// HTTP.DELETE
// "/maps/{carteID}"
// const MAP_DELETE_URL = 'https://localhost:8089/maps/1';
export default function MiniatureMap(props){

    const URL_MAP_DELETE = '/maps/'+props.map.id;
    const URL_CANVAS_OPEN = "/edit/"+props.map.id;

    // Callback pour notifier le catalogue si ce composant a lancé une suppression (reload le catalogue)
    const notifyCatalogue = props.notifyCatalogue;

    const [open, setOpen] = useState(false);
    const [showLoadingOverlay, setShowLoadingOverlay] = useState(false)
    const handleOpenDialog  = () => setOpen(true);
    const handleCloseDialog = () => setOpen(false);

    const handleDelete = () => deleteMap(props.map.id);
    const deleteMap = () => {
        setShowLoadingOverlay(true)

        fetch(URL_MAP_DELETE,{
            method  : 'DELETE',
            headers : api_request_headers
        })
            .then(response => {
                setShowLoadingOverlay(false)
                setNotificationType("success")
                setNotificationMessage("La carte a été supprimée")
                setSnackbarOpenRemove(true)
                console.log("MAP HAS BEEN DELETED", response.text());
                notifyCatalogue(); // Send Callback to catalogue to refresh
            })
            .catch(function(error){
                setShowLoadingOverlay(false)
                setNotificationType("error")
                setNotificationMessage("La carte n'a pas pu être supprimée")
                setSnackbarOpenRemove(true)
                console.error("DELETE MAP FAILED", error);
            });
        handleCloseDialog();
    }

    const [showGestionDroit, setShowGestionDroit] = useState(false);
    const handleShowGestionDroit  = () => setShowGestionDroit(true);
    const handleCloseGestionDroit = () => setShowGestionDroit(false);
    const gestionDroitIfTrue = <Droit map={props.map} updateMiniature={handleCloseGestionDroit} />;

    const [snackbarOpenRemove, setSnackbarOpenRemove] = useState(false);
    const [notificationMessage, setNotificationMessage] = useState("");
    const [notificationType, setNotificationType] = useState("");
    const handleSnackbarCloseRemove = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setSnackbarOpenRemove(false);
    };

    return (
        <div className={"MiniatureMap"}>
            { showLoadingOverlay ? <LoadingScreen/> : null }
            <div className={"custom-card d-flex align-items-center justify-content-center"}>
                {showGestionDroit ? gestionDroitIfTrue : <></>}
                <div className={"custom-card-overlay d-flex align-items-center"}>
                    <Row xs={1} className={"custom-card-overlay-container"}>
                        {/*<Col>*/}
                        {/*    <Row className={"custom-card-overlay-row"}>*/}
                        {/*        <Col className={"custom-card-overlay-row-label"}>Éditer</Col>*/}
                        {/*        <Col xs={4}><FontAwesomeIcon icon="fa-solid fa-pen" /></Col>*/}
                        {/*    </Row>*/}
                        {/*</Col>*/}
                        <Col>
                            <Row className={"custom-card-overlay-row"}>
                                <Col className={"custom-card-overlay-row-label"}><Link className={"custom-dom-link"} to={URL_CANVAS_OPEN}>Consulter</Link></Col>
                                <Col xs={4}><FontAwesomeIcon icon="fa-solid fa-magnifying-glass" /></Col>
                            </Row>
                        </Col>
                        {
                            ( props.map.permission === 'OWNER' ) ?
                                <Col onClick={handleShowGestionDroit}>
                                    <Row className={"custom-card-overlay-row"}>
                                        <Col className={"custom-card-overlay-row-label"}>Gestion des droits</Col>
                                        <Col xs={4}><FontAwesomeIcon icon="fa-solid fa-gear" /></Col>
                                    </Row>
                                </Col>
                                :
                                <></>
                        }
                        <Col>
                            <Row onClick={handleOpenDialog}
                                 style={{color: 'red'}} className={"custom-card-overlay-row"}>
                                <Col  className={"custom-card-overlay-row-label"}>Supprimer</Col>
                                <Col xs={4}><FontAwesomeIcon icon="fa-solid fa-trash" /></Col>
                            </Row>
                        </Col>
                    </Row>
                </div>
                <div className={"custom-card-title"}>
                    {props.map.name}
                </div>
                <div className={"custom-card-background-img"}>
                    <img src={logo} alt={"map background"}/>
                </div>
            </div>
            <Dialog onClose={handleCloseDialog} open={open}>
                <DialogTitle>Êtes-vous sûr ?</DialogTitle>
                <div style={{width : '100%', padding: '5px 20px'}} className={"d-flex justify-content-between"}>
                    <Button onClick={handleCloseDialog} variant={"text"}>Non</Button>
                    <Button style={{backgroundColor: 'red'}} onClick={handleDelete} variant={"contained"}>Oui</Button>
                </div>
            </Dialog>

            <Snackbar
                open={snackbarOpenRemove}
                autoHideDuration={5000}
                onClose={handleSnackbarCloseRemove}
            >
                <Alert onClose={handleSnackbarCloseRemove} severity={notificationType} sx={{ width: '100%' }}>
                    {notificationMessage}
                </Alert>
            </Snackbar>
        </div>
    );
}