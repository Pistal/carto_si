import "./Dropzone.css"
import {useDropzone} from "react-dropzone";
import {useEffect, useState} from "react";

export default function Dropzone(props) {
    const {acceptedFiles, getRootProps, getInputProps, fileRejections} = useDropzone({
        disabled: props.disabled ?? false,
        accept: ".xlsx",
        maxFiles: 1,
        multiple: false
    });
    const [file, setFile] = useState()
    useEffect(() => {
        if(acceptedFiles.length === 0) return;
        props.onFileSelected(acceptedFiles[0]);
        setFile(acceptedFiles[0]);
    }, [acceptedFiles]);

    return (
        <div className={"dropzone_container"}>
            <div {...getRootProps({className: 'dropzone'})}
                 style={{borderColor: props.disabled ?? false ? "gray" : fileRejections.length > 0 ? 'red' : `#00915A`}}>
                <input {...getInputProps()} />
                <span>
                    {
                        file !== undefined
                            ? file.name
                            : 'Glisser un fichier excel (extension .xlsx) ici'
                    }
                </span>
            </div>
        </div>
    )
}