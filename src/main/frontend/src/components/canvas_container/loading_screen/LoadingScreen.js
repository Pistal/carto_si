import './LoadingScreen.css';
import {CircularProgress} from "@mui/material";

export default function LoadingScreen(){

    return(
        <div className={"LoadingScreen"}>
            <CircularProgress id={"circular-progress"} color="success" />
        </div>
    );
}