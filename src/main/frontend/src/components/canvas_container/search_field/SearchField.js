import './SearchField.css';
import {useEffect, useState} from "react";

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {useSigma} from "react-sigma-v2";

export default function SearchField(props){

    const filters = props.fields;

    const sigma = useSigma();

    // searchbar content
    const [search, setSearch] = useState("");
    const [values, setValues] = useState([]);
    const [selected, setSelected] = useState(null);

    const refreshValues = () => {
        const newValues = [];
        const lcSearch = search.toLowerCase();
        if (!selected && search.length > 1) {
            sigma.getGraph().forEachNode((key, attributes) => {
                if (!attributes.hidden && attributes.label && attributes.label.toLowerCase().indexOf(lcSearch) === 0)
                    newValues.push({ id: key, label: attributes.label });
            });
        }
        setValues(newValues);
    };

    // Refresh values when search is updated:
    useEffect(() => refreshValues(), [search]); // eslint-disable-line react-hooks/exhaustive-deps

    // Refresh values when filters are updated (but wait a frame first):
    useEffect(() => {
        requestAnimationFrame(refreshValues);
    }, [filters]); // eslint-disable-line react-hooks/exhaustive-deps

    useEffect(() => {
        if (!selected) return;

        sigma.getGraph().setNodeAttribute(selected, "highlighted", true);
        const nodeDisplayData = sigma.getNodeDisplayData(selected);

        if (nodeDisplayData) {
            sigma.getCamera().animate(
                {...nodeDisplayData, ratio: 0.05},
                {
                    duration: 600,
                },
            );
        }

        return () => {
            sigma.getGraph().setNodeAttribute(selected, "highlighted", false);
        };
    }, [selected]); // eslint-disable-line react-hooks/exhaustive-deps

    const onInputChange = (e) => {
        const searchString = e.target.value;
        const valueItem = values.find((value) => value.label === searchString);
        if (valueItem) {
            setSearch(valueItem.label);
            setValues([]);
            setSelected(valueItem.id);
        } else {
            setSelected(null);
            setSearch(searchString);
        }
    };

    const onKeyPress = (e) => {
        if (e.key === "Enter" && values.length) {
            setSearch(values[0].label);
            setSelected(values[0].id);
        }
    };

    return(
        <div className="search-wrapper panel">
            <div className={"d-flex justify-content-center align-items-center"}>
                <FontAwesomeIcon id={"searchbar-icon"} icon={"fa-solid fa-magnifying-glass"} />
                <input
                    id={"searchbar-building"}
                    type="search"
                    placeholder="Rechercher"
                    list="nodes"
                    value={search}
                    onChange={onInputChange}
                    onKeyPress={onKeyPress}
                />
            </div>
            <datalist id="nodes">
                {
                    values.map((value, _) => (
                        <option key={value.id} value={value.label}>
                            {value.label}
                        </option>
                    ))
                }
            </datalist>
        </div>
    );
}