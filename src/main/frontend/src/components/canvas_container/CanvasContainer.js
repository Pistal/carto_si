import './CanvasContainer.css';
import {useEffect, useState} from "react";
import LoadingScreen from "./loading_screen/LoadingScreen";

import "react-sigma-v2/lib/react-sigma-v2.css";
import {SigmaContainer} from "react-sigma-v2";
import GraphData from "./GraphData";
import SearchField from "./search_field/SearchField";
import FilterPanel from "./filter_panel/FilterPanel";
import IdentityCard from "./identity_card/IdentityCard";
import GraphSettingsController from "./GraphSettingsController";
//import Legend from "./legend/Legend";

// import Data from './second.json';
// const json = Data;
// console.log("DATA", json);

export default function CanvasContainer(props) {

    const MAP_FETCH_URL = '/maps/'+props.mapId;

    const createFilter = (filters) => {
        const obj = {};
        filters.forEach( (filter) => {
            obj[filter.name] = {};
            filter.attributes.forEach( attr => {
                const tmp = {};
                tmp[attr] = true;
                obj[filter.name] = {...obj[filter.name], ...tmp};
            });
        })
        return obj;
    }

    const [ready, setReady] = useState(false);
    const [fetched, setFetched] = useState(false);

    const [showIdentity, setShowIdentity] = useState(false);
    const [currentBuilding, setCurrentBuilding] = useState(null);
    const [hoveredNode, setHoveredNode] = useState(null);
    // const [showLegend, setShowLegend] = useState(false);

    //{ buildings : [], links : [] }
    const [map, setMap] = useState({});
    const [filtersState, setFiltersState] = useState({});
    // const [map, setMap] = useState(json);
    // const [filtersState, setFiltersState] = useState( createFilter(map.filters) );

    /**
     * Loads map to display */
    useEffect( () => {
        console.log("FETCHING MAP");
        const fetchCarte = () => {
            fetch(MAP_FETCH_URL)
                .then(response => response.json())
                .then(data => {
                    console.log(data);
                    setMap(data);
                    setFiltersState( createFilter(data.filters) );
                    setFetched(true);
                })
                .catch(function(error){
                    console.error("FETCH MAP FAILED", error);
                });
        };

        fetchCarte();
    },[]); // eslint-disable-line react-hooks/exhaustive-deps

    const handleToggleFilter = (category, filterName) => {
        console.log("toggleFilter", filterName);
        console.log("BEFORE", filtersState);
        // setFiltersState((filters) => (filters[filter] ? omit(filters, filter) : { ...filters, [filter]: false }));
        const tmp = {...filtersState};
        tmp[category][filterName] = !tmp[category][filterName];
        setFiltersState(tmp);
    };

    return (
        <>
            <div className={"CanvasContainer"}>
                {
                    !ready ? <LoadingScreen /> : null
                }
                {
                    !fetched ? null :
                    <div className={"sigma-adapter"}>
                        <SigmaContainer graphOptions={{ type: "directed" }} initialSettings={{defaultEdgeType: "arrow"}}>
                            <GraphSettingsController hoveredNode={hoveredNode} />
                            <GraphData map={map} filters={filtersState} handleCurrentBuilding={(building) => setCurrentBuilding(building)} handleShowIdentity={(value) => setShowIdentity(value)} setHoveredNode={setHoveredNode} onReady={() => setReady(true)}/>
                            <div className="panels-left">
                                <SearchField filters={filtersState} />
                                <FilterPanel
                                    rawFilters={map.filters}
                                    filters={filtersState}
                                    toggleFilter={handleToggleFilter}
                                />
                            </div>
                            {showIdentity && (
                                <div className={"panels-right"}>
                                    <IdentityCard building={currentBuilding} closeIdentity={() => setShowIdentity(false)} />
                                </div>
                            )}

                            {/*{showLegend && (*/}
                            {/*    <div className={"panels-bottom"}>*/}
                            {/*        <Legend rawFilters={map.filters} />*/}
                            {/*    </div>*/}
                            {/*)}*/}
                        </SigmaContainer>
                    </div>
                }
            </div>
        </>
    );
}