import { useSigma, useRegisterEvents} from "react-sigma-v2";
import {useEffect} from "react";
import forceAtlas2 from "graphology-layout-forceatlas2";

function getMouseLayer() {
    return document.querySelector(".sigma-mouse");
}

export default function GraphData(props){
    const map = props.map;
    const filters = props.filters;

    const handleCurrentBuilding = props.handleCurrentBuilding;
    const handleShowIdentity    = props.handleShowIdentity;

    // Buildings already use id from 1 to n buildings, districts cannot share those same IDs inside Sigma.
    const PREFIX_D = 'd-';

    const BASE_SIZE_D = 1;
    const BASE_SIZE_B = 1;

    const sigma = useSigma();
    const graph = sigma.getGraph();
    const registerEvents = useRegisterEvents();

    useEffect(() => {
        if (!graph || !map) return;
        console.log("BUILDING MAP");

        // Create districts
        map.districts.forEach( (district) => {
            const d_id = PREFIX_D+district.id;
            graph.addNode(d_id, district);

            // TODO if its the graph setup, size will be null
            if( district.size === null ){
                graph.setNodeAttribute(d_id, "size", BASE_SIZE_D);
            }
        });

        // Reloop to avoid sigma crash (pure safety measure in case where we try to add edge with a district that sigma didn't add yet which leads to a crash)
        map.districts.forEach( (district) => {
            if( typeof(district.id_parent) === 'undefined' || district.id_parent === null ) {
                return;
            }
            graph.addEdge(PREFIX_D+district.id, PREFIX_D+district.id_parent, {size: 2});
        });

        // Create buildings, link them between districts aa between themselves
        map.buildings.forEach( (node) => {
            // Add building to graph
            graph.addNode(node.id, node);

            // TODO if its the graph setup, size will be null
            if( node.size === null ) {
                graph.setNodeAttribute(node.id, "size", BASE_SIZE_B);
            }
            // Connect node to its district, abort if it has no data about it
            if( typeof(node.id_district) === 'undefined' || node.id_district === null ) {
                return;
            }
            if( graph.hasNode(PREFIX_D+node.id_district) ){
                graph.addEdge(node.id, PREFIX_D+node.id_district);
            }
        });
        // Link each building
        map.links.forEach( (edge) => {
            graph.addEdge(edge.id_building_1, edge.id_building_2, {color: edge.color});
        });

        // ONLY IF BUILDING MAP FOR THE FIRST TIME
        // Growing nodes depending on the number of edges connected to them
        graph.forEachNode( (idNode) => {
            const degree = graph.degree(idNode);
            const tmp = degree * 0.1;
            const newSize = graph.getNodeAttribute(idNode, "size") + tmp;
            graph.setNodeAttribute(idNode, "size", newSize);
        });

        // ForceAtlas2 will arrange the graph and place nodes correctly
        forceAtlas2.assign(graph, {
            iterations: 500,
            barnesHutOptimize: true,
        });
        props.onReady();
        return () => graph.clear();
    }, [graph, map]);

    /**
     * Apply filters to graphology
     */
    useEffect(() => {
        console.log("APPLY FILTER");

        // function to test a node if any filter is hiding it
        const showNode = (attr) => {
            // For each filter in filterState, test each node
            for (const a of (attr ?? [])) {
                if( !filters[a.attributeKey][a.attributeValue] ) {
                    return false;
                }
            }
            return true;
        }
        graph.forEachNode((id, node) => graph.setNodeAttribute(id, "hidden", !showNode(node.attributes)) );
    }, [graph, filters]);

    /**
     * Initialize here settings that require to know the graph and/or the sigma
     * instance:
     */
    useEffect(() => {
        const isDistrict = (node) => {
            const str = node.toString();
            return str.startsWith(PREFIX_D);
        } ;

        registerEvents({
            clickNode({ node }) {
                console.log("CLICK ON ", node);
                if( isDistrict(node) ){
                    // TODO
                } else { // Is building
                    if (!graph.getNodeAttribute(node, "hidden")) {
                        const tmp = graph.getNodeAttributes(node);
                        handleCurrentBuilding(tmp);
                        handleShowIdentity(true);
                    }
                }

            },
            enterNode({ node }) {
                props?.setHoveredNode(node);
                const mouseLayer = getMouseLayer();
                if (mouseLayer) mouseLayer.classList.add("mouse-pointer");
            },
            leaveNode() {
                props?.setHoveredNode(null);
                const mouseLayer = getMouseLayer();
                if (mouseLayer) mouseLayer.classList.remove("mouse-pointer");
            },
        });
    }, []);

    return null;
}