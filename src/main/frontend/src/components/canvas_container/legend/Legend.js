import './Legend.css';

export default function Legend(props){

    const rawFilters = props.rawFilters;

    return (
        <div className="d-flex flex-row justify-content-around Legend">
            <div id={"legend-structure"} className={"legend-item"}>
                <span className={"legend-title"}>Structure</span>
                <div id={"legend-structure-content"} className={"d-flex flex-column"}>
                    {
                        rawFilters.map( rawFilter => (
                            <span>{rawFilter.name}</span>
                        ))
                    }
                </div>
            </div>
            <div id={"legend-colors"} className={"legend-item"}>
                <span className={"legend-title"}>Couleurs</span>
                <div className={"d-flex flex-column"}>
                    <div className={"legend-color"}>lol</div>
                </div>
            </div>
        </div>
    );
}