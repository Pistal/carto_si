import './FilterPanel.css';
import { Accordion, AccordionDetails, AccordionSummary } from '@mui/material';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';

export default function FilterPanel(props){

    const rawFilters = props.rawFilters;
    rawFilters.sort((a, b) => {
        if(a.name < b.name) return -1
        else if (a.name > b.name) return 1
        return 0
    })

    return(
        <div className="panel FilterPanel">
            <span className={"panel-title"}>Filtres</span>
            <div id={"filter-list"} className={"d-flex flex-column"}>
                {
                    rawFilters.map( rawFilter => {
                        return <Accordion>
                            <AccordionSummary expandIcon={<ExpandMoreIcon />}>{rawFilter.name}</AccordionSummary>
                            <AccordionDetails>
                                {
                                    rawFilter.attributes.sort().map((name, _) =>
                                        <div className={"d-flex justify-content-between"}>
                                            <span className={"filter-name"} style={{fontStyle: name === "" ? "italic" : "normal"}}>
                                                {name === "" ? "Non renseigné" : name}
                                            </span>
                                            <input
                                                type="checkbox"
                                                checked={props.filters[rawFilter.name][name] || false}
                                                onChange={() => props.toggleFilter(rawFilter.name, name)}
                                                id={`tag-${props.name}`}
                                            />
                                        </div>
                                    )
                                }
                            </AccordionDetails>
                        </Accordion>
                    })
                }
            </div>
        </div>
    );
}