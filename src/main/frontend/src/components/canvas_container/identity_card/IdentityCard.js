import './IdentityCard.css';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {Table} from "react-bootstrap";

export default function IdentityCard(props){

    const building = props.building;
    const nothingToShow = building.attributes.length === 0;
    const attributes = building.attributes;
    attributes.sort((a, b) => {
        if(a.attributeKey < b.attributeKey) return -1;
        if(a.attributeKey > b.attributeKey) return 1;
        return 0;
    })
    return (
        <>
            {
                nothingToShow ? (
                    <div className={"IdentityCard"}>
                        <div id={"identity-card-title-container"} className={"d-flex align-items-center justify-content-between"}>
                            <span id={"card-title"}>{building.label}</span>
                            <div id={"card-close-button-container"}><span id={"card-close-button"} onClick={props.closeIdentity}><FontAwesomeIcon icon={"fa-solid fa-x"}/></span></div>
                        </div>
                        <div id={"identity-card-table-container-empty"} className={"d-flex justify-content-center"}>
                            Aucun attribut
                        </div>
                    </div>
                ) : (
                    <div className={"IdentityCard"}>
                        <div id={"identity-card-title-container"} className={"d-flex align-items-center justify-content-between"}>
                            <span id={"card-title"}>{building.label}</span>
                            <div id={"card-close-button-container"}><span id={"card-close-button"} onClick={props.closeIdentity}><FontAwesomeIcon icon={"fa-solid fa-x"}/></span></div>
                        </div>
                        <div id={"identity-card-table-container"}>
                            <Table striped hover>
                                <tbody>
                                {attributes.map((attribute, _) => (
                                    <tr>
                                        <td>{attribute.attributeKey} </td>
                                        <td>{attribute.attributeValue === '' ? "-" : attribute.attributeValue}</td>
                                    </tr>
                                ))}
                                </tbody>
                            </Table>
                        </div>
                    </div>
                )
            }
        </>
    );
}