import "./CreateMapFormModal.css"
import {useState} from "react";
import {Button, Modal, Step, StepLabel, Stepper} from "@mui/material";
import CreationStep from "./steps/CreationStep";
import StructurationStep from "./steps/StructurationStep";
import LinksStep from "./steps/LinksStep";
import ConfigurationStep from "./steps/ConfigurationStep";
import {useDispatch, useSelector} from "react-redux";
import {
    getData,
    setBuildingAttribute,
    setBuildingColorAttribute,
    setDistrictAttributes, setLinks
} from "../../redux/creationFormSlice";
import {api_request_headers} from "../../tools/api_request_header";
import axios from "axios";
import _ from "lodash-contrib";
import LoadingScreen from "../canvas_container/loading_screen/LoadingScreen";

export default function CreateMapFormModal(props) {
    const dispatch = useDispatch()
    const [activeStep, setActiveStep] = useState(0);
    const [showLoadingOverlay, setShowLoadingOverlay] = useState(false)
    const creationFormData = useSelector(getData)

    const disableNext = () => {
        switch (activeStep) {
            case 0: return creationFormData.name === null || creationFormData.attributes === null
            case 1: return creationFormData.districtAttributes.map(da => da?.values?.length ?? 0).reduce((a, b) => a * b, 1) > 1000
            case 2: return false
            case 3: return false
            default: return false
        }
    }

    const createMap = () => {
        setShowLoadingOverlay(true)
        const data = _.cloneDeep(creationFormData)
        delete data['attributes']
        delete data['isEmpty']
        axios.post(
            '/configuration/createMap/',
            JSON.stringify(data),
            {
                headers: {
                    ...api_request_headers,
                    'Content-Type': `application/json`,
                }
            },
        ).then(result => {
            setShowLoadingOverlay(false)
            if(result.status === 200) {
                props.onMapCreated();
                props.onClose()
            }
        }).catch(error => {
            setShowLoadingOverlay(false)
        })
    }

    const handleNext = () => {
        if(activeStep === 3) {
            createMap()
        } else {
            setActiveStep((prevActiveStep) => prevActiveStep + 1);
        }
    };

    const handleBack = () => {
        switch(activeStep) {
            case 1:
                dispatch(setDistrictAttributes([creationFormData.attributes[0]]))
                dispatch(setBuildingAttribute(creationFormData.attributes[1]))
                dispatch(setBuildingColorAttribute(creationFormData.attributes[2]))
                break
            case 2:
                break
            case 3:
                dispatch(setLinks([]))
                break
        }
        setActiveStep((prevActiveStep) => prevActiveStep - 1);
    };

    const handleReset = () => {
        setActiveStep(0);
    };

    const greenColor = getComputedStyle(document.documentElement)
        .getPropertyValue('--main-green-color');

    return (
        <Modal open={props.open} onClose={props.onClose} closeAfterTransition>
            <div className={"modal_content"}>
                { showLoadingOverlay ? <LoadingScreen/> : null }
                <div className={"modal_body_container"}>
                    <div className={"modal_body d-flex flex-column"}>
                        <div className={"flex-grow-1"}>
                            <Stepper color={greenColor} activeStep={activeStep} orientation="vertical">
                                <Step key={"creation_step"}>
                                    <StepLabel>Création de la carte</StepLabel>
                                    <CreationStep/>
                                </Step>
                                <Step key={"structuration_step"}>
                                    <StepLabel>Structuration de la carte</StepLabel>
                                    <StructurationStep/>
                                </Step>
                                <Step key={"configuration_step"}>
                                    <StepLabel>Configuration de la carte</StepLabel>
                                    <ConfigurationStep/>
                                </Step>
                                <Step key={"links_step"}>
                                    <StepLabel>Liens</StepLabel>
                                    <LinksStep/>
                                </Step>
                            </Stepper>
                        </div>
                        <div className={"w-100 mt-5 d-flex justify-content-end"}>
                            <Button className={"map_form_button"} disabled={activeStep === 0} onClick={handleBack} title={"Back"}>
                                Retour
                            </Button>
                            <Button className={"map_form_button"} disabled={disableNext()} onClick={handleNext} title={"Next"}>
                                {activeStep < 3 ? "Suivant" : "Valider"}
                            </Button>
                        </div>
                    </div>
                </div>
            </div>
        </Modal>
    )
}