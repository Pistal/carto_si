import "./LinksStep.css"
import Container from "react-bootstrap/Container";
import {Button, FormControl, FormGroup, InputLabel, MenuItem, Select, StepContent, TextField} from "@mui/material";
import {useState} from "react";
import {SketchPicker} from "react-color";
import $ from "jquery";
import _ from "lodash-contrib";
import {useDispatch, useSelector} from "react-redux";
import {getData, getLinks, setLinks} from "../../../redux/creationFormSlice";

export default function LinksStep() {
    const dispatch = useDispatch()
    const links = useSelector(getLinks)
    const formData = useSelector(getData)
    const [colorUpdater, setColorUpdater] = useState((color) => {})
    const [currentValueColor, setCurrentValueColor] = useState()
    const [linkName, setLinkName] = useState("")
    const [firstAttribute, setFirstAttribute] = useState(null)
    const [secondAttribute, setSecondAttribute] = useState(null)

    const convertColorToCss = (color) => {
        return `rgba(${color.r},${color.g},${color.b},${color.a})`
    }

    const addLink = () => {
        const link = {
            name: linkName,
            firstAttribute: firstAttribute,
            secondAttribute: secondAttribute,
            color: {
                r: _.random(256),
                g: _.random(256),
                b: _.random(256),
                a: 255,
            },
            lineType: 0,
            thickness: 1
        }
        setLinkName("")
        setFirstAttribute(null)
        setSecondAttribute(null)
        dispatch(setLinks([...links, link]))
    }

    const handleClickColor = (link) => {
        setCurrentValueColor(link.color)
        setColorUpdater(() => color => {
            const linksCopy = _.cloneDeep(links)
            linksCopy.find(l => l.name === link.name).color = color
            dispatch(setLinks(linksCopy))
        })
        const offset = $(`#link_color_tag_${link.name.replaceAll(" ", "_")}`).offset()
        $("#popover").css({left: offset.left, top: offset.top, display: "block"})
    };

    const handleClose = () => {
        $("#popover").css({display: "none"})
    };

    const buildLink = (link) => {
        return (
            <div>
                <div>{link.name}</div>
                <div className={"link_settings d-flex"}>
                    <div
                        id={`link_color_tag_${link.name}`}
                        className={"link_color_tag"}
                        style={{backgroundColor: convertColorToCss(link.color)}}
                        onClick={() => handleClickColor(link)}
                    />
                    {/*<input*/}
                    {/*    type='int'*/}
                    {/*    step="1"*/}
                    {/*    min='1'*/}
                    {/*    max='5'*/}
                    {/*    className='link_thickness_selector'*/}
                    {/*    value={link.thickness}*/}
                    {/*    onChange= {(event) => {*/}
                    {/*        const linksCopy = _.cloneDeep(links)*/}
                    {/*        linksCopy.find(l => l.name === link.name).thickness = event.target.value*/}
                    {/*        dispatch(setLinks(linksCopy))*/}
                    {/*    }}*/}
                    {/*/>*/}
                    {/*<Select*/}
                    {/*    className={"link_line_select align-items-center"}*/}
                    {/*    value={link.lineType}*/}
                    {/*    onChange={event => {*/}
                    {/*        const linksCopy = _.cloneDeep(links)*/}
                    {/*        linksCopy.find(l => l.name === link.name).lineType = event.target.value*/}
                    {/*        dispatch(setLinks(linksCopy))*/}
                    {/*    }}>*/}
                    {/*    <MenuItem value={0}>*/}
                    {/*        <div className={"link_straight_line"}/>*/}
                    {/*    </MenuItem>*/}
                    {/*    <MenuItem value={1}>*/}
                    {/*        <div className={"link_dashed_line"}/>*/}
                    {/*    </MenuItem>*/}
                    {/*</Select>*/}
                </div>
            </div>
        )
    }

    const isLinkAlreadyCreated = () => {
        return !formData.links
            .map(link => [link.firstAttribute, link.secondAttribute])
            .every(
                tuple => !tuple.every(
                    attribute => [firstAttribute, secondAttribute].includes(attribute)
                )
            )
    }

    const cannotAddLink = () => {
        const emptyFields = linkName === "" || firstAttribute === "" || secondAttribute === ""
        const nameAlreadyUsed = links
            .map(v => v.name)
            .includes(linkName)
        const linkAlreadyCreated = isLinkAlreadyCreated()
        return emptyFields || nameAlreadyUsed || (links.length > 0 && linkAlreadyCreated)
    }

    const isAttributeUsed = a => !(formData.districtAttributes.map(attr => attr?.attribute).includes(a?.attribute))

    const getRemainingAttributes = (currentAttribute, attributeToIgnore) => {
        if(formData.attributes === null) return []
        return [
            ...formData.attributes.filter(attr => isAttributeUsed(attr) || attr?.attribute === attributeToIgnore?.attribute),
            currentAttribute
        ].sort((a, b) => {
            if (a?.attribute < b?.attribute) return -1
            else if (a?.attribute > b?.attribute) return 1
            return 0
        })
    }

    return (
        <StepContent>
            <Container className={"step"}>
                <div className={"d-flex h-100"}>
                    <FormGroup className={"link_form"}>
                        <TextField
                            className={"name_field"}
                            required
                            label="Nom"
                            value={linkName}
                            onChange={(e) => setLinkName(e.target.value)}
                        />
                        <FormControl className={"w-100 mt-3"}>
                            <InputLabel class={"mt-3"} id="first_attribute_select_label">Attribut identifiant</InputLabel>
                            <Select
                                labelId="first_attribute_select_label"
                                id="first_attribute_select"
                                className={"attribute_select"}
                                label="Attribut 1"
                                renderValue={value => value === null ? "" : value}
                                value={firstAttribute}
                                onChange={e => setFirstAttribute(e.target.value)}
                            >
                                {
                                    getRemainingAttributes(firstAttribute, secondAttribute).map(attr => (
                                        <MenuItem value={attr?.attribute}>{attr?.attribute}</MenuItem>
                                    ))
                                }
                            </Select>
                        </FormControl>
                        <FormControl className={"w-100 mt-3"}>
                            <InputLabel class={"mt-3"} id="second_attribute_select_label">Attribut étranger</InputLabel>
                            <Select
                                labelId="second_attribute_select_label"
                                id="second_attribute_select"
                                className={"attribute_select"}
                                label="Attribut 2"
                                value={secondAttribute}
                                onChange={e => setSecondAttribute(e.target.value)}
                            >
                                {
                                    getRemainingAttributes(secondAttribute, firstAttribute).map(attr => (
                                        <MenuItem value={attr?.attribute}>{attr?.attribute}</MenuItem>
                                    ))
                                }
                            </Select>
                        </FormControl>

                        <Button disabled={cannotAddLink()} className={"mt-3 map_form_button"} onClick={addLink}>Ajouter</Button>
                    </FormGroup>

                    <div className={"vertical_separator"}/>

                    <div className={"link_list"}>
                        <div className={"link_list_box_container"}>
                            <div className={"link_list_box p-1"}>
                                {
                                    links.map(buildLink)
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </Container>
            <div id={"popover"}>
                <div id={"cover"} onClick={handleClose}/>
                <SketchPicker
                    color={currentValueColor}
                    onChange={(color) => {
                        setCurrentValueColor(color.rgb)
                        colorUpdater(color.rgb)
                    }}
                />
            </div>
        </StepContent>
    )
}