import "./StructurationStep.css";
import {StepContent} from "@mui/material";
import {Button, CloseButton, Form, InputGroup} from "react-bootstrap";
import _ from "lodash-contrib";
import Container from "react-bootstrap/Container";
import {useDispatch, useSelector} from "react-redux";
import {
    getAttributes,
    getBuildingAttribute,
    getBuildingColorAttribute,
    getDistrictAttributes,
    setBuildingAttribute,
    setBuildingColorAttribute,
    setDistrictAttributes
} from "../../../redux/creationFormSlice";
import {mapAttributeWithRandomColor} from "../../../tools/redux_formatter";

export default function StructurationStep() {
    const dispatch = useDispatch();
    const attributes = useSelector(getAttributes)
    const districtAttributes = useSelector(getDistrictAttributes)
    const buildingAttribute = useSelector(getBuildingAttribute);
    const buildingColorAttribute = useSelector(getBuildingColorAttribute);

    const addDistrict = () => {
        const defaultValue = _.cloneDeep(getRemainingAttributes()[0])
        dispatch(setDistrictAttributes(([...districtAttributes, mapAttributeWithRandomColor(defaultValue)])))
    }

    const changeDistrict = (index, value) => {
        const entry = _.cloneDeep(attributes.find(a => a.attribute === value))
        const copy = _.cloneDeep(districtAttributes)
        copy[index] = mapAttributeWithRandomColor(entry)
        dispatch(setDistrictAttributes(copy))
    }

    const removeDistrict = (index) => {
        const copy = districtAttributes.filter((_, i) => {
            return i !== index
        })
        dispatch(setDistrictAttributes(copy))
    }

    const isAttributeUsed = a => !(districtAttributes.map(attr => attr?.attribute).includes(a?.attribute)) &&
        a?.attribute !== buildingAttribute?.attribute &&
        a?.attribute !== buildingColorAttribute?.attribute

    const getRemainingAttributes = (currentAttribute) => {
        if(attributes === null) return []
        return [...attributes.filter(isAttributeUsed), currentAttribute].sort((a, b) => {
            if (a?.attribute < b?.attribute) return -1
            else if (a?.attribute > b?.attribute) return 1
            return 0
        })
    }

    const handleBuildingAttributeChange = (e) => {
        const attribute = e.target.value
        const entry = _.cloneDeep(attributes.find(a => a.attribute === attribute))
        dispatch(setBuildingAttribute(entry))
    }

    const handleBuildingColorAttributeChange = (e) => {
        const attribute = e.target.value
        const entry = mapAttributeWithRandomColor(_.cloneDeep(attributes.find(a => a.attribute === attribute)))
        dispatch(setBuildingColorAttribute(entry))
    }

    const numberOfDistrictsToCreate = districtAttributes.map(da => da?.values?.length ?? 0).reduce((a, b) => a * b, 1)

    return (
        <StepContent>
            <Container className={"mt-3 step"}>
                <Form.Group className={"d-flex flex-column flex-fill"} id={"create_map_form"}>
                    {
                        _.range(districtAttributes.length)
                            .map(i =>
                                <div key={"district_" + i}>
                                    <InputGroup className="district d-flex align-items-center mb-3">
                                        <InputGroup.Text id={"profile_input_"+i}>
                                            Attribut de quartier {i + 1}
                                        </InputGroup.Text>
                                        <Form.Select
                                            value={districtAttributes[i]?.attribute}
                                            className={"district_field"}
                                            id={"district_select_"+i}
                                            aria-describedby={"profile_input_"+i}
                                            onChange={e => changeDistrict(i, e.target.value)}
                                            placeholder={"Choisir un attribut"}>
                                            {
                                                getRemainingAttributes(districtAttributes[i]).map(a => (
                                                    <option value={a?.attribute}>{a?.attribute}</option>
                                                ))
                                            }
                                        </Form.Select>
                                        <CloseButton className={'ms-3'} onClick={() => removeDistrict(i)}/>
                                    </InputGroup>
                                </div>
                            )
                    }

                    <div className={"mb-3 text-center"} style={{color: numberOfDistrictsToCreate > 1000 ? "red" : "green"}}>
                        Nombre de quartiers générés {numberOfDistrictsToCreate} (max. 1000)
                    </div>

                    <Button className={"add_district_button"} type="button" onClick={addDistrict}>
                        Ajouter un quartier
                    </Button>
                    <div className={"d-flex mt-3"}>
                        <div className={"building me-1 flex-grow-1"}>
                            <InputGroup className="d-flex align-items-center mb-3">
                                <InputGroup.Text id="building_input_label">
                                    Attribut du batiment
                                </InputGroup.Text>
                                <Form.Select
                                    value={buildingAttribute?.attribute}
                                    className={"building_field"}
                                    aria-describedby="building_input_label"
                                    onChange={handleBuildingAttributeChange}
                                    placeholder={"Choisir un attribut"}>
                                    {
                                        getRemainingAttributes(buildingAttribute).map(a => (
                                            <option value={a?.attribute}>{a?.attribute}</option>
                                        ))
                                    }
                                </Form.Select>
                            </InputGroup>
                        </div>
                        <div className={"building_color flex-grow-1"}>
                            <InputGroup className="d-flex align-items-center mb-3">
                                <InputGroup.Text id="building_color_label">
                                    Attribut de la couleur
                                </InputGroup.Text>
                                <Form.Select
                                    value={buildingColorAttribute?.attribute}
                                    className={"building_color_field"}
                                    aria-describedby="building_color_label"
                                    onChange={handleBuildingColorAttributeChange}
                                    placeholder={"Choisir un attribut"}>
                                    {
                                        getRemainingAttributes(buildingColorAttribute).map(a => (
                                            <option value={a?.attribute}>{a?.attribute}</option>
                                        ))
                                    }
                                </Form.Select>
                            </InputGroup>
                        </div>
                    </div>
                </Form.Group>
            </Container>
        </StepContent>
    )
}