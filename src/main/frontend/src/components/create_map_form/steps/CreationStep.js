import './CreationStep.css'
import {StepContent, TextField} from "@mui/material";
import Dropzone from "../../dropzone/Dropzone";
import {useCallback, useEffect, useState} from "react";
import axios from "axios";
import {api_request_headers} from "../../../tools/api_request_header";
import {useDispatch, useSelector} from "react-redux";
import {clearAttributes, getAttributes, getName, setAttributes, setName} from "../../../redux/creationFormSlice";
import _ from "lodash-contrib";
import LoadingScreen from "../../canvas_container/loading_screen/LoadingScreen";

export default function CreationStep() {
    const storedMapName = useSelector(getName)
    const attributes = useSelector(getAttributes)
    const dispatch = useDispatch()

    const [file, setFile] = useState()
    const [mapName, setMapName] = useState("")
    const [isNameValid, setIsNameValid] = useState(false)
    const [showLoadingOverlay, setShowLoadingOverlay] = useState(false)

    const sendNameVerification = (name) => {
        axios.get(
            `/configuration/verifyName/${name}`,
            {
                headers: api_request_headers
            }
        ).then(result => {
            if(result.status === 200) {
                setIsNameValid(result.data.valid)
                if(result.data.valid) {
                    dispatch(setName(name))
                } else {
                    setMapName(storedMapName)
                }
            } else {
                setIsNameValid(false)
                setMapName(storedMapName)
            }
        }).catch(_ => {
            setIsNameValid(false)
            setMapName(storedMapName)
        })
    }

    const verificationDebounce = useCallback(_.debounce(sendNameVerification, 2000), [])

    const handleNameChange = (e) => {
        setMapName(e.target.value)
    }

    useEffect(() => {
        if(mapName === "") {
            setIsNameValid(false)
        } else {
            verificationDebounce(mapName)
        }
    }, [mapName])

    useEffect(() => {
        if(mapName === "" || file === undefined) return;

        setShowLoadingOverlay(true)

        const data = new FormData();
        data.append('file', file);
        data.append('mapName', mapName)

        axios.post(
            `/configuration/submitExcel/`,
            data,
            {
                headers: {
                    ...api_request_headers,
                    'Content-Type': `multipart/form-data;boundary=CartoSIBoundary`,
                }
            }
        ).then(result => {
            setShowLoadingOverlay(false)
            if(result.status === 200) {
                dispatch(setAttributes(result.data))
            } else {
                dispatch(clearAttributes())
            }
        }).catch(_ => {
            setShowLoadingOverlay(false)
            dispatch(clearAttributes())
        })
    }, [file])

    return (
        <StepContent>
            { showLoadingOverlay ? <LoadingScreen/> : null }
            <TextField
                className={"name_field mb-3"}
                required
                label="Nom"
                value={mapName}
                onChange={handleNameChange}
            />
            <Dropzone disabled={!isNameValid} onFileSelected={setFile}/>
            {
                attributes == null ? null : (
                    <div className={"mt-3 text-center"}>
                        <p>Nombre de colonnes: {attributes.length}</p>
                        <p>Nombre de lignes: {Math.max(...(attributes.map(el => el.values.length)))}</p>
                    </div>
                )
            }
        </StepContent>
    )
}