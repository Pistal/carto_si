import './ConfigurationStep.css'
import {StepContent} from "@mui/material";
import {useState} from "react";
import Container from "react-bootstrap/Container";
import {SketchPicker} from "react-color";
import $ from "jquery"
import _ from "lodash-contrib";
import {useDispatch, useSelector} from "react-redux";
import {
    getBuildingColorAttribute,
    getDistrictAttributes, setBuildingColorAttribute,
    setDistrictAttributes
} from "../../../redux/creationFormSlice";

export default function ConfigurationStep() {
    const dispatch = useDispatch()
    const buildingColor = useSelector(getBuildingColorAttribute)
    const districts = useSelector(getDistrictAttributes)
    const [selectedDistrict, setSelectedDistrict] = useState()
    const [colorUpdater, setColorUpdater] = useState((color) => {})
    const [currentValueColor, setCurrentValueColor] = useState()

    const convertColorToCss = (color) => {
        return `rgba(${color?.r},${color?.g},${color?.b},${color?.a})`
    }

    const setDistrictColorUpdater = (value) => {
        setCurrentValueColor(value?.color)
        setColorUpdater(() => color => {
            const districtsCopy = _.cloneDeep(districts)
            const selectedDistrictCopy = districtsCopy.find(d => d.attribute === selectedDistrict.attribute)
            selectedDistrictCopy.values.find(v => v.name === value.name).color = color

            dispatch(setDistrictAttributes(districtsCopy))

            setSelectedDistrict(selectedDistrictCopy)
        })
    }

    const setBuildingColorUpdater = (value) => {
        setCurrentValueColor(value?.color)
        setColorUpdater(() => color => {
            const buildingColorCopy = _.cloneDeep(buildingColor)
            buildingColorCopy.values.find(v => v.name === value.name).color = color
            dispatch(setBuildingColorAttribute(buildingColorCopy))
        })
    }

    const handleClick = (prefix, value, updater) => {
        updater(value)
        const offset = $(`\#${prefix}_${value?.name.replaceAll(' ', '_')}`).offset()
        $("#popover").css({left: offset.left, top: offset.top, display: "block"})
    };

    const handleClose = () => {
        $("#popover").css({display: "none"})
    };

    const buildDistrict = (district) => {
        return (
            <div key={district?.attribute} className={"option"} onClick={() => {
                setSelectedDistrict(district)
            }}>
                {district?.attribute}
            </div>
        )
    }

    const buildValue = (prefix, value, updater) => {
        return (
            <div key={value?.name} id={`${prefix}_${value?.name?.replaceAll(' ', '_')}`}
                 className={"option d-flex align-items-center"}
                 onClick={() => handleClick(prefix, value, updater)}>
                <div
                    className={"color_tag me-3"}
                    style={{backgroundColor: convertColorToCss(value?.color)}}
                />
                <span style={{fontStyle: value?.name === "" ? "italic" : "normal"}}>{value?.name === "" ? "Non renseigné" : value?.name}</span>
            </div>
        )
    }

    return (
        <StepContent>
            <Container id={"configuration"} className={"mt-3 step d-flex flex-column align-items-center"}>
                <div id={"district_section"} className={"d-flex flex-row"}>
                    <div id={"districts"} className={"configuration_list_box_container me-3"}>
                        <div className={"configuration_list_box p-1"}>
                            {
                                _.interpose(
                                    [...(districts ?? [])]?.sort((a, b) => {
                                        if(a.attribute < b.attribute) return -1;
                                        if(a.attribute > b.attribute) return 1;
                                        return 0;
                                    })?.map(buildDistrict),
                                    <div className={"thin-separator"}/>
                                )
                            }
                        </div>
                    </div>
                    <div id={"district_values"} className={"configuration_list_box_container"}>
                        <div className={"configuration_list_box p-1"}>
                            {
                                selectedDistrict !== undefined
                                    ? _.interpose(
                                        [...(selectedDistrict?.values ?? [])]?.sort((a, b) => {
                                            if(a.name < b.name) return -1;
                                            if(a.name > b.name) return 1;
                                            return 0;
                                        })?.map(v => buildValue(`${selectedDistrict.attribute.replaceAll(" ", "_")}_district_value`, v, setDistrictColorUpdater)) ?? [],
                                        <div className={"thin-separator"}/>
                                    )
                                    : null
                            }
                        </div>
                    </div>
                </div>

                <div className={"separator"}/>

                <span>Couleur des batiments</span>
                <div id={"building_values"} className={"configuration_list_box_container"}>
                    <div className={"configuration_list_box p-1"}>
                        {
                            _.interpose(
                                [...(buildingColor?.values ?? [])].sort((a, b) => {
                                    if(a.name < b.name) return -1;
                                    if(a.name > b.name) return 1;
                                    return 0;
                                })?.map(v => buildValue('building_value', v, setBuildingColorUpdater)) ?? [],
                                <div className={"thin-separator"}/>
                            )
                        }
                    </div>
                </div>
                <div id={"popover"}>
                    <div id={"cover"} onClick={handleClose}/>
                    <SketchPicker
                        color={currentValueColor}
                        onChange={(color) => {
                            setCurrentValueColor(color.rgb)
                            colorUpdater(color.rgb)
                        }}
                    />
                </div>
            </Container>
        </StepContent>
    )
}