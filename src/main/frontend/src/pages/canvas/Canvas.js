import './Canvas.css';
import {useParams} from "react-router-dom";
import CanvasContainer from "../../components/canvas_container/CanvasContainer";

export default function Canvas(props){
    props.updateTitle();

    // Récupération depuis l'URL des paramètres (défini dans App.js)
    const { mapId } = useParams();

    return(
        <div className="Canvas">
            <CanvasContainer mapId={mapId} />
        </div>
    );
}