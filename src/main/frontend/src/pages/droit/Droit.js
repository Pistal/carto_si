import './Droit.css';
import {Col, Row, Modal, Button} from "react-bootstrap";
import {forwardRef, useEffect, useState} from "react";
import './Droit.css';
import { library } from "@fortawesome/fontawesome-svg-core";
import {faSquarePlus} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import {api_request_headers} from "../../tools/api_request_header";
import Snackbar from '@mui/material/Snackbar';
import MuiAlert from '@mui/material/Alert';
import AddIcon from '@mui/icons-material/Add';
import {Autocomplete, IconButton, TextField} from "@mui/material";

library.add(faSquarePlus);

const Alert = forwardRef(function Alert(props, ref) {
    return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
});

export default function Droit(props) {

    const map = props.map;
    const updateMiniature = props.updateMiniature;

    const USERS_FETCH_URL = '/users';
    //const USERS_ON_MAP_FETCH_URL = '/user/maps/{mapId}';
    const USER_ON_MAP_URL = '/user/maps/';
    const USERS_ON_MAP_FETCH_URL = '/user/maps/'+map.id;

    const [showModal, setShowModal] = useState(true);
    const handleCloseModal = () => {
        setShowModal(false);
        updateMiniature();
    }

    const [snackbarOpen, setSnackbarOpen] = useState(false);
    const handleSnackbarClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setSnackbarOpen(false);
    };

    const [successOp, setSuccessOp] = useState(false);
    const [lastOp, setLastOp] = useState('ADD');

    const [usersOnMap, setUsersOnMap] = useState(null);
    const [usersOnMapReady, setUsersOnMapReady] = useState(false);

    const [modificationCount, setModificationCount] = useState(0);

    const handleDeleteUserPermission = (userId) => {
        setLastOp('DELETE');
        const fetchDeleteUserPermission = () => {
            fetch(USER_ON_MAP_URL+userId+'/'+map.id, {
                method  : 'DELETE',
                headers : api_request_headers,
            })
                .then(response => {
                    console.log("PERMISSION DELETED", response.text());
                    setUsersOnMapReady(false);
                    setSuccessOp(true);
                    setSnackbarOpen(true);
                    setModificationCount(modificationCount+1);
                })
                .catch(error => {
                    console.error("PERMISSION DELETION FAILED", error);
                    setSuccessOp(false);
                    setSnackbarOpen(true);
                });
        }
        fetchDeleteUserPermission();
    }

    const fetchUpdateUserOnMap = (user, permission) => {
        setLastOp('ADD');
        if(permission !== 'EDITOR' && permission !== 'VISITOR') {
            console.error("NON-VALID PERMISSION : ", permission);
            return;
        }
        fetch(USER_ON_MAP_URL, {
            method  : 'POST',
            headers : {...api_request_headers, "content-type" : "application/json"},
            body    : JSON.stringify({
                userId     : user.id,
                mapId      : map.id,
                permission : permission
            })
        })
            .then(response => {
                if(response.status === 200) {
                    console.log("PERMISSION DELETED", response);
                    setModificationCount(modificationCount + 1);
                    setSuccessOp(true);
                    setSnackbarOpen(true);
                    setCurrentUser(null);
                } else {
                    console.error("PERMISSION DELETION FAILED");
                    setSuccessOp(false);
                    setSnackbarOpen(true);
                }
            })
            .catch(function(error){
                console.error("PERMISSION DELETION FAILED", error);
                setSuccessOp(false);
                setSnackbarOpen(true);
            });
    };

    useEffect( () => {
        console.log("FETCH USERS ON MAP");
        const fetchUsersOnMap = () => {
            fetch(USERS_ON_MAP_FETCH_URL)
                .then(response => response.json())
                .then(data => {
                    console.log(data);
                    setUsersOnMap(data);
                    setUsersOnMapReady(true);
                })
                .catch(function(error){
                    console.error("FETCH USERS ON MAP FAILED", error);
                });
        };
        fetchUsersOnMap();
    }, [modificationCount]);

    const [allUsers, setAllUsers] = useState(null);
    const [allUsersReady, setAllUsersReady] = useState(false);

    useEffect( () => {
        console.log("FETCH ALL USERS REGISTERED");
        const fetchAllUsers = () => {
            fetch(USERS_FETCH_URL)
                .then(response => response.json())
                .then(data => {
                    console.log(data);
                    setAllUsers(data);
                    setAllUsersReady(true);
                })
                .catch(function(error){
                    console.error("FETCH ALL USERS REGISTERED FAILED", error);
                });
        };
        fetchAllUsers();
    }, []);

    const [currentUser, setCurrentUser] = useState(null);
    const addPermission = () => fetchUpdateUserOnMap(currentUser, 'VISITOR')


    return (
        <div className="Droits">
            <Modal show={showModal} onHide={handleCloseModal}>
                <Modal.Header className={"justify-content-center"}>
                    <span id={"droits-title"}>{map.name}</span>
                </Modal.Header>
                <Modal.Body>
                    <div className="Droits-container">
                        <div className={"droits-section"}>
                            <div className="d-flex justify-content-center droits-section-title-container">
                                <span className="droits-section-title">Ajouter un utilisateur</span>
                            </div>
                            <div className="d-flex justify-content-center">
                                <div className={"w-50"}>
                                    <div className="d-flex justify-content-center align-items-center">
                                        <Autocomplete
                                            value={currentUser}
                                            disablePortal
                                            className="me-3 flex-grow-1"
                                            options={allUsers?.filter(user => !(usersOnMap?.map(u => u?.id)?.includes(user?.id) ?? false)) ?? []}
                                            getOptionLabel={(user) => `${user.firstName} ${user.lastName}`}
                                            renderOption={(props, user) => (
                                                <div {...props}>{user.firstName} {user.lastName}</div>
                                            )}
                                            renderInput={(params) => <TextField {...params} label="Choisir un utilisateur" />}
                                            onChange={(event, value) => setCurrentUser(value)}
                                        />

                                        <IconButton disabled={currentUser === null} onClick={addPermission}>
                                            <AddIcon color={"success"}/>
                                        </IconButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className={"droits-section"}>
                            <div className="d-flex justify-content-center droits-section-title-container">
                                <span className={"droits-section-title"}>Utilisateurs de la carte</span>
                            </div>
                            <div className="list_box_container">
                                <div className={"list_box"}>
                                    <Row xs={1}>
                                        {usersOnMapReady ? (
                                            <>
                                                {
                                                    usersOnMap.length === 0 ? (
                                                        <Col className={"d-flex justify-content-center"}>
                                                            <span id={"no-users-text"}>Aucun utilisateur n'a été ajouté</span>
                                                        </Col>
                                                    ) : (
                                                        <>
                                                            {
                                                                usersOnMap.map((user, _) => (
                                                                    <Col style={{marginBottom : "10px"}}>
                                                                        <div className={"d-flex justify-content-between"}>
                                                                            <div className={"users"}>{user.name}</div>
                                                                            <div>
                                                                                <button type="button" className="close" aria-label="Close" onClick={(_) => handleDeleteUserPermission(user.id)}>
                                                                                    <span aria-hidden="true">&times;</span>
                                                                                </button>
                                                                            </div>
                                                                        </div>
                                                                    </Col>
                                                                ))
                                                            }
                                                        </>
                                                    )
                                                }
                                            </>
                                        ) : (
                                            <Col className={"d-flex justify-content-center"}>
                                                <span id={"loading-text"}>Chargement...</span>
                                            </Col>
                                        )}
                                    </Row>
                                </div>
                            </div>
                        </div>
                    </div>
                </Modal.Body>
                <Modal.Footer className={"d-flex justify-content-end"}>
                    <Button variant={"text"} className={"custom-button"} onClick={handleCloseModal} >
                        Fermer
                    </Button>
                </Modal.Footer>
            </Modal>
            <Snackbar
                open={snackbarOpen}
                autoHideDuration={5000}
                onClose={handleSnackbarClose}
            >
                {
                    successOp ? (
                        lastOp === 'ADD' ? (
                            <Alert onClose={handleSnackbarClose} severity="success" sx={{ width: '100%' }}>
                                Utilisateur ajouté/mis à jour
                            </Alert>
                        ) : (
                            <Alert onClose={handleSnackbarClose} severity="success" sx={{ width: '100%' }}>
                                Utilisateur supprimé
                            </Alert>
                        )
                    ) : (
                        <Alert onClose={handleSnackbarClose} severity="error" sx={{ width: '100%' }}>
                            Échec
                        </Alert>
                    )
                }
            </Snackbar>
        </div>
    );
}