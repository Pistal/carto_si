import './Catalogue.css';
import {Col, Container, Form, Row} from "react-bootstrap";
import MiniatureMap from "../../components/miniature/MiniatureMap";
import CreateMapFormModal from "../../components/create_map_form/CreateMapFormModal";
import {forwardRef, useEffect, useReducer, useRef, useState} from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Snackbar from "@mui/material/Snackbar";
import MuiAlert from "@mui/material/Alert";

const Alert = forwardRef(function Alert(props, ref) {
    return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
});

export default function Catalogue(props){
    props.updateTitle();

    const MAP_FETCH_URL = '/maps';

    const [maps, setMaps] = useState([]);

    // HANDLE MODAL VIEW : Création d'une carte
    const [showModal, setShowModal] = useState(false);
    const handleShowModal  = () => setShowModal(true);
    const handleCloseModal = () => setShowModal(false);

    // SEARCH BAR
    const [filter, setFilter] = useState("");
    const applyFilter = str => filter === '' ? true : str.toLowerCase().includes(filter.toLowerCase());
    const handleFilter = (event) => setFilter(event.target.value);

    // TRI - https://stackoverflow.com/questions/59040989/usestate-with-a-lambda-invokes-the-lambda-when-set
    const useLambda = initVal => {
        const setter = useRef((__, next) => ({ val: next })).current
        const [wrapper, dispatch] = useReducer(setter, { val: initVal })
        const state = wrapper.val
        return [state, dispatch]
    };
    const [currentSort, setCurrentSort] = useLambda((a, b) => a.name.localeCompare(b.name, 'fr', {ignorePunctuation: true}));
    const handleSort = event => {
        console.log(event);
        const value = event.target.value;

        if(value === 'alpha'){
            setCurrentSort((a, b) => a.name.localeCompare(b.name, 'fr', {ignorePunctuation: true}));
        } else if( value === 'reverse_alpha'){
            setCurrentSort((a, b) => b.name.localeCompare(a.name, 'fr', {ignorePunctuation: true}));
        } else if( value === 'date'){
            setCurrentSort((a, b) => {
                if(a.creation_date.getDate() < b.creation_date.getDate()) return -1;
                if(a.creation_date.getDate() > b.creation_date.getDate()) return 1;
                return 0;
            });
        } else if( value === 'reverse_date'){
            setCurrentSort((a, b) => {
                if(a.creation_date.getDate() > b.creation_date.getDate()) return -1;
                if(a.creation_date.getDate() < b.creation_date.getDate()) return 1;
                return 0;
            });
        }
    };

    // FILTRE VIA PERMISSIONS
    const [filterPermissions, setFilterPermissions] = useState(
        new Array(3).fill(true)
    );
    const FILTER_PERMISSIONS = {
        "OWNER"   : 0,
        "EDITOR"  : 1,
        "VISITOR" : 2
    }
    const applyFilterPermission = map => filterPermissions[FILTER_PERMISSIONS[map.permission]];
    const handlePermissionFilter = (pos) => {
        const updatedPermissions = filterPermissions.map( (item, i) => i === pos ? !item : item);
        // filterPermissions[pos] = !filterPermissions[pos];
        setFilterPermissions(updatedPermissions);
    };

    const [deletedMapCount, setDeletedMapCount] = useState(0);

    const fetchMaps = () => {
        fetch(MAP_FETCH_URL) //, {method: 'GET'}
            .then(response => response.json())
            .then(data => {
                console.log(data);
                // CONVERT DATE : String to Date : Date.parse(str)
                for (let i = 0; i < data.length; i++) {
                    data[i].creation_date = new Date(data[i].creation_date);
                }
                // Update maps list
                setMaps(data);
            })
            .catch(function(error){
                console.error("FETCH MAPS FAILED", error);
            });

    }

    useEffect(() => {
        console.log("FETCHING MAPS");
        fetchMaps();
    }, [deletedMapCount]);

    const onMapCreated = () => {
        setNotificationType("success");
        setNotificationMessage("La carte a été créée")
        setSnackbarOpenAdd(true);
        fetchMaps();
    }

    const onMapCreateError = () => {
        setNotificationType("error");
        setNotificationMessage("La carte n'a pas pu être créée")
        setSnackbarOpenAdd(true);
    }

    const modal = <CreateMapFormModal open={showModal} onClose={handleCloseModal} onMapCreated={onMapCreated} onError={onMapCreateError}/>;


    const [snackbarOpenAdd, setSnackbarOpenAdd] = useState(false);
    const [notificationMessage, setNotificationMessage] = useState("");
    const [notificationType, setNotificationType] = useState("");
    const handleSnackbarCloseAdd = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setSnackbarOpenAdd(false);
    };

    return (
        <div className="Catalogue">
            {showModal ? modal : <></>}
            <div className="Catalogue-header">
                <Form className={"d-flex align-items-center justify-content-between"}>
                    <div className="Catalogue-header-item w-50">
                        <Form.Group controlId="searchBar">
                            <Form.Control className={"custom-searchbar"} type="email" placeholder="Rechercher" onChange={handleFilter} />
                        </Form.Group>
                    </div>
                    <div className="Catalogue-header-item">
                        <Form.Group className={"d-flex align-items-center"} controlId="sortList">
                            <label className={"me-3"}><span>Trier par</span></label>
                            <div>
                                <Form.Select className={"custom-select"} onChange={handleSort}>
                                    <option value="alpha">Ordre alphabétique</option>
                                    <option value="reverse_alpha">Ordre alphabétique décroissant</option>
                                    <option value="date">Du plus récent</option>
                                    <option value="reverse_date">Du moins récent</option>
                                </Form.Select>
                            </div>
                        </Form.Group>
                    </div>
                    <div className="Catalogue-header-item">
                        <Form.Group controlId="formBasicCheckbox">
                            <div className={"d-flex"}>
                                <label><span>Rôles :</span></label>
                                <div className={"ms-3"}>
                                    <Form.Check className={"mb-0"} inline type="checkbox" label="Propriétaire" checked={filterPermissions[0]} onChange={() => handlePermissionFilter(0)}/>
                                    {/*<Form.Check inline type="checkbox" label="Éditeur"      checked={filterPermissions[1]} onChange={() => handlePermissionFilter(1)}/>*/}
                                    <Form.Check className={"mb-0"} inline type="checkbox" label="Visiteur"     checked={filterPermissions[2]} onChange={() => handlePermissionFilter(2)}/>
                                </div>
                            </div>
                        </Form.Group>
                    </div>
                </Form>
            </div>
            <Container className="Catalogue-container" fluid>
                <Row xs={2} lg={3} xl={4} xxl={5}>
                    <Col className={"card-container justify-content-center"} >
                        <div id={"new-map-button"} onClick={handleShowModal}>
                            <FontAwesomeIcon id={"new-map-button-x"} icon="fa-solid fa-plus" size={"10x"} />
                        </div>
                    </Col>
                    {
                        maps
                            ?.sort(currentSort)
                            ?.filter(map => applyFilter(map.name))
                            ?.filter(map => applyFilterPermission(map))
                            ?.map((map, id) => (
                                <Col id={id} className={"card-container justify-content-center"}>
                                    <MiniatureMap
                                        map={map}
                                        notifyCatalogue={() => setDeletedMapCount(deletedMapCount + 1) }
                                    />
                                </Col>
                            ))
                    }
                </Row>
            </Container>
            <Snackbar
                open={snackbarOpenAdd}
                autoHideDuration={5000}
                onClose={handleSnackbarCloseAdd}
            >
                <Alert onClose={handleSnackbarCloseAdd} severity={notificationType} sx={{ width: '100%' }}>
                    {notificationMessage}
                </Alert>
            </Snackbar>
        </div>
    );
}