import _ from "lodash-contrib";

export const mapAttributeWithRandomColor = (attr) => {
    return {
        attribute: attr.attribute,
        values: attr.values.map(v => {
            return {
                name: v,
                color: {
                    r: _.random(256),
                    g: _.random(256),
                    b: _.random(256),
                    a: 255,
                }
            }
        })
    }
}