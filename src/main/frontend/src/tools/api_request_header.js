import Cookies from 'js-cookie';

const csrfToken = Cookies.get('XSRF-TOKEN');
export const api_request_headers = {
    'X-XSRF-TOKEN': csrfToken
};