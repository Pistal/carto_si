import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import 'bootstrap/dist/css/bootstrap.min.css';
import fontawesome from '@fortawesome/fontawesome';

import { Provider } from 'react-redux';

import store from './redux/store';


// add icons here : add in both those lines ..
import {faGear, faMagnifyingGlass, faPen, faPlus, faTrash, faX, faPowerOff} from '@fortawesome/free-solid-svg-icons';
fontawesome.library.add(faPen, faMagnifyingGlass, faGear, faTrash, faPlus, faX, faPowerOff);

// Remove all logs. You must comment this line if you are in dev mode
console.log = function() {};

ReactDOM.render(
    <Provider store={store}>
        <React.StrictMode>
            <App />
        </React.StrictMode>
    </Provider>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
