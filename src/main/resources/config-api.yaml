openapi: 3.0.0

info:
  version: 0.0.1
  title: CartoSI Service
  description: The CartoSI service

servers:
  - url: https://localhost:8089/api
    description: This is where my api is


tags:
  - name: "User"
  - name: "Canvas"
  - name: "Catalogue"
  - name: "Configuration"
  - name: "Droit"

paths:
  /logout:
    get:
      summary: Logout user
      tags:
        - "User"
      operationId: logout
      responses:
        "200":
          description: Successfully logged out
        "403":
          description: Unauthorized
  /user:
    get:
      summary: Load user information given by keycloak
      tags:
        - "User"
      operationId: getUser
      responses:
        "200":
          description: User information was found
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/UserPOJO'
  /maps:
    get:
      summary: Load all the maps the user possesses
      tags:
        - "Catalogue"
      description: The request needs the userId to make the request on the database.
      operationId: getMaps
      responses:
        "200":
          description: All maps are returned
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/MapUserPOJO'
  /maps/{mapId}:
    get:
      summary: Load a map
      tags:
        - "Catalogue"
      description: Load map
      operationId: getMap
      parameters:
        - in: path
          name: mapId
          schema:
            type: integer
            format: int64
          required: true
          description: ID of the map
      responses:
        "200":
          description: Map is loaded
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/MapCanvasPOJO'
        "400":
          description: BAD REQUEST
        "401":
          description: UNAUTH
    delete:
      summary: Delete a map
      tags:
        - "Catalogue"
      description: Deleting a map if the user is the owner and remove the permission anyway
      operationId: deleteMap
      parameters:
        - in: path
          name: mapId
          schema:
            type: integer
            format: int64
          required: true
          description: ID of the map to delete
      responses:
        "200":
          description: Map has been deleted
        "400":
          description: BAD REQUEST
        "401":
          description: UNAUTH
        "404":
          description: Map not found
  /users:
    get:
      summary: Find users with their name
      tags:
        - "Droit"
      description: The request needs the username to make the request on the database.
      operationId: getAllUsers
      responses:
        "200":
          description: All users are loaded
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: "#/components/schemas/UserPOJO"
        "401":
          description: UNAUTH

  /user/maps/{mapId}:
    get:
      summary: To retrieve all users with map rights
      tags:
        - "Droit"
      description: The request needs the map Id to make the request on the database.
      operationId: getAllUserWithPermissionOnMap
      parameters:
        - in: path
          name: mapId
          schema:
            type: integer
            format: int64
          required: true
          description: ID of the map
      responses:
        "200":
          description: All users are loaded
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/DroitUserPOJO'
        "401":
          description: UNAUTH
  /user/maps:
    post:
      summary: Add or update permission on given Map
      tags:
        - "Droit"
      operationId: addPermissions
      requestBody:
        description: Add permission
        required: true
        content:
          application/json:
            schema:
              type: object
              properties:
                userId:
                  type: integer
                  format: int64
                mapId:
                  type: integer
                  format: int64
                permission:
                  type: string
      responses:
        "200":
          description: Permission added
        "401":
          description: UNAUTH
        "404":
          description: User or Map not found
  /user/maps/{userId}/{mapId}:
    delete:
      summary: Delete a user
      tags:
        - "Droit"
      description: Deleting a user permission from map
      operationId: deleteUserPermission
      parameters:
        - in: path
          name: userId
          schema:
            type: integer
            format: int64
            required: true
            description: ID of the user
        - in: path
          name: mapId
          schema:
            type: integer
            format: int64
            required: true
            description: ID of the map
      responses:
        "200":
          description: User has been deleted
        "401":
          description: UNAUTH
        "404":
          description: User or Map not found

  /configuration/{configurationId}:
    get:
      summary: Load a map
      tags:
        - "Configuration"
      description: Get a configuration with its id
      operationId: getProfile
      parameters:
        - in: path
          name: configurationId
          schema:
            type: integer
            format: int64
          required: true
          description: ID of the profile to get
      responses:
        "200":
          description: Profile is loaded
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ConfigurationProfilePOJO'
        "400":
          description: BAD REQUEST
        "401":
          description: UNAUTH
  /configuration/verifyName/{mapName}:
    get:
      summary: Verify map name unicity
      tags:
        - "Configuration"
      operationId: verifyName
      parameters:
        - in: path
          name: mapName
          schema:
            type: string
          required: true
          description: Name of the map
      responses:
        "200":
          description: Returns boolean about name validity
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/NameValidityPOJO"
  /configuration/submitExcel/:
    post:
      summary: submitExcel and give attributs
      tags:
        - "Configuration"
      operationId: submitExcel
      requestBody:
        description: Excel file
        required: true
        content:
          multipart/form-data:
            schema:
              type: object
              properties:
                mapName:
                  type: string
                file:
                  type: string
                  format: binary
      responses:
        "200":
          description: FILE SUCCESSFULLY UPLOADED
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: "#/components/schemas/AttributeValuePOJO"
        "400":
          description: WRONG FORMAT
        "401":
          description: UNAUTHORIZED
        "403":
          description: NOT ALLOWED
  /configuration/createMap/:
    post:
      summary: Create a map
      tags:
        - "Configuration"
      operationId: createMap
      requestBody:
        description: JSON describing the map to create
        required: true
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/CreateMapPOJO"
      responses:
        "200":
          description: MAP SUCCESSFULLY CREATED
        "400":
          description: BAD REQUEST
        "401":
          description: UNAUTHORIZED
        "403":
          description: NOT ALLOWED


  /canvas/identityBuilding/{mapId}/{buildingId}:
    get:
      summary: Get attributes of building
      tags:
        - "Canvas"
      operationId: getBuildingAttributes
      parameters:
        - in: path
          name: buildingId
          schema:
            type: integer
            format: int64
            required: true
            description: ID of the building
        - in: path
          name: mapId
          schema:
            type: integer
            format: int64
            required: true
            description: ID of the maps
      responses:
        "200":
          description: Building Found
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/AttributePOJO'
        "404":
          description: Building not found
components:
  schemas:
    UserPOJO:
      description: User POJO to send to the front app
      type: object
      properties:
        id:
          description: Keycloak Id of the user
          type: integer
          format: int64
        firstName:
          description: User's firstName
          type: string
        lastName:
          description: User's lastName
          type: string
    MapPOJO:
      description: This is the map Model
      type: object
      properties:
        id:
          description: Id of the map
          type: integer
          format: int64
        name:
          description: Map's name
          type: string
        last_user:
          description: Map's last user
          type: string
        creation_date:
          description: Map's date of creation
          type: string
        # TODO : add members ?
    MapUserPOJO:
      allOf:
        - $ref: '#/components/schemas/MapPOJO'
        - type: object
        - description: Map POJO in the catalogue
          properties:
            permission:
              description: Permission of the user on the Map
              type: string
    DroitUserPOJO:
      type: object
      description: A user with its permission on a map
      properties:
        id:
          description: userId
          type: integer
          format: int64
        name:
          type: string
        permission:
          description: permission on the map of the user
          type: string

    ConfigurationProfilePOJO:
      description: This is the configuration profile
      type: object
      properties:
        author:
          description: name of author
          type: string
    MapCanvasPOJO:
      description: Form of the map loaded when the canvas is used
      type: object
      properties:
        header:
          description: name of author
          type: string
        buildings:
          type: array
          items:
            $ref: '#/components/schemas/BuildingPOJO'
        links:
          type: array
          items:
            $ref: '#/components/schemas/LinkPOJO'
        filters:
          type: array
          items:
            $ref: '#/components/schemas/FilterPOJO'
        districts:
          type: array
          items:
            $ref: '#/components/schemas/DistrictPOJO'
    BuildingPOJO:
      description: Building POJO
      type: object
      properties:
        id:
          type: integer
          format: int64
        id_district:
          type: integer
          format: int64
        label:
          type: string
        x:
          type: number
          format: double
        y:
          type: number
          format: double
        color:
          type: string
        size:
          type: integer
        attributes:
          type: array
          items:
            $ref: '#/components/schemas/AttributePOJO'
    DistrictPOJO:
      description: District POJO
      type: object
      properties:
        id:
          type: integer
          format: int64
        id_parent:
          type: integer
          format: int64
        id_district_value:
          type: integer
          format: int64
        label:
          type: string
        x:
          type: number
          format: double
        y:
          type: number
          format: double
        color:
          type: string
        size:
          type: integer
        icon:
          type: string
    LinkPOJO:
      description: Link POJO
      type: object
      properties:
        id_building_1:
          type: integer
          format: int64
        id_building_2:
          type: integer
          format: int64
        size:
          type: integer
        color:
          type: string
    FilterPOJO:
      description: Filter POJO
      type: object
      properties:
        name:
          type: string
        attributes:
          type: array
          items:
            type: string
    AttributePOJO:
      description: Attribute POJO
      type: object
      properties:
        attributeKey:
          type: string
        attributeValue:
          type: string
    AttributeValuePOJO:
      description: Attribute and value POJO
      type: object
      properties:
        attribute:
          type: string
        values:
          type: array
          items:
            type: string
    NameValidityPOJO:
      description: Name validity POJO
      type: object
      properties:
        valid:
          type: boolean
    CreateMapLinkPOJO:
      description: Links used to create a map
      type: object
      properties:
        name:
          type: string
        firstAttribute:
          type: string
        secondAttribute:
          type: string
        color:
          $ref: "#/components/schemas/ColorPOJO"
        lineType:
          type: integer
        thickness:
          type: integer
    CreateMapBuildingColorAttributePOJO:
      description: Building color attribute used to create a map
      type: object
      properties:
        attribute:
          type: string
        values:
          type: array
          items:
            type: object
            properties:
              name:
                type: string
              color:
                $ref: "#/components/schemas/ColorPOJO"



    CreateMapBuildingAttributePOJO:
      description: Building attribute used to create a map
      type: object
      properties:
        attribute:
          type: string
        values:
          type: array
          items:
            type: string
    CreateMapDistrictAttributesPOJO:
      description: Districts attributes used to create a map
      type: object
      properties:
        attribute:
          type: string
        values:
          type: array
          items:
            type: object
            properties:
              name:
                type: string
              color:
                $ref: "#/components/schemas/ColorPOJO"
    ColorPOJO:
      description: Color but in an object
      type: object
      properties:
        r:
          type: integer
        g:
          type: integer
        b:
          type: integer
        a:
          type: integer


    CreateMapPOJO:
      description: Object used to create a map
      type: object
      properties:
        name:
          type: string
        districtAttributes:
          type: array
          items:
            $ref: "#/components/schemas/CreateMapDistrictAttributesPOJO"
        buildingAttribute:
          $ref: "#/components/schemas/CreateMapBuildingAttributePOJO"
        buildingColorAttribute:
          $ref: "#/components/schemas/CreateMapBuildingColorAttributePOJO"
        links:
          type: array
          items:
            $ref: "#/components/schemas/CreateMapLinkPOJO"
