# AVANT:
#FROM openjdk:17
#EXPOSE 8080
#ADD target/spring-boot-docker.jar spring-boot-docker.jar
#ENTRYPOINT ["java", "-jar", "/spring-boot-docker.jar"]

#
# Build stage
#
FROM maven:3.8.4-openjdk-17-slim
RUN echo " -- BUILDING DOCKER BACKEND IMAGE -- "
WORKDIR /app
COPY src /app/src
COPY pom.xml /app
COPY package.json /app
COPY scripts /app/scripts
COPY config /app/config
COPY .env /app

# TODO COPY FRONTEND FILES

RUN echo "-- BUILDING APP WITH MAVEN --"
RUN mvn clean package
#
# Run stage
#
RUN echo " -- RUNNING APP -- "
EXPOSE 8089
ENTRYPOINT ["java", "-jar", "/app/target/spring-boot-docker.jar"]

