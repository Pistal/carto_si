ALTER TABLE IF EXISTS ONLY public.value DROP CONSTRAINT IF EXISTS fk_value_attribute;
ALTER TABLE IF EXISTS ONLY public.user_permission DROP CONSTRAINT IF EXISTS fk_permission_user;
ALTER TABLE IF EXISTS ONLY public.user_permission DROP CONSTRAINT IF EXISTS fk_permission_header;
ALTER TABLE IF EXISTS ONLY public.link_type DROP CONSTRAINT IF EXISTS fk_lt_config;
ALTER TABLE IF EXISTS ONLY public.link_type DROP CONSTRAINT IF EXISTS fk_lt_a2;
ALTER TABLE IF EXISTS ONLY public.link_type DROP CONSTRAINT IF EXISTS fk_lt_a1;
ALTER TABLE IF EXISTS ONLY public.header DROP CONSTRAINT IF EXISTS fk_header_user;
ALTER TABLE IF EXISTS ONLY public.header DROP CONSTRAINT IF EXISTS fk_header_config;
ALTER TABLE IF EXISTS ONLY public.district_value DROP CONSTRAINT IF EXISTS fk_dv_value;
ALTER TABLE IF EXISTS ONLY public.district DROP CONSTRAINT IF EXISTS fk_district_dv;
ALTER TABLE IF EXISTS ONLY public.district DROP CONSTRAINT IF EXISTS fk_district_district;
ALTER TABLE IF EXISTS ONLY public.depth DROP CONSTRAINT IF EXISTS fk_depth_config;
ALTER TABLE IF EXISTS ONLY public.depth DROP CONSTRAINT IF EXISTS fk_depth_attribute;
ALTER TABLE IF EXISTS ONLY public.configuration DROP CONSTRAINT IF EXISTS fk_config_owner;
ALTER TABLE IF EXISTS ONLY public.building DROP CONSTRAINT IF EXISTS fk_building_district;
ALTER TABLE IF EXISTS ONLY public.building DROP CONSTRAINT IF EXISTS fk_building_bv;
ALTER TABLE IF EXISTS ONLY public.building DROP CONSTRAINT IF EXISTS fk_building_key;
ALTER TABLE IF EXISTS ONLY public.building_property DROP CONSTRAINT IF EXISTS fk_bp_value;
ALTER TABLE IF EXISTS ONLY public.building_property DROP CONSTRAINT IF EXISTS fk_bp_building;
ALTER TABLE IF EXISTS ONLY public.building_link DROP CONSTRAINT IF EXISTS fk_bl_lt;
ALTER TABLE IF EXISTS ONLY public.building_link DROP CONSTRAINT IF EXISTS fk_bl_b2;
ALTER TABLE IF EXISTS ONLY public.building_link DROP CONSTRAINT IF EXISTS fk_bl_b1;
ALTER TABLE IF EXISTS ONLY public.building_value DROP CONSTRAINT IF EXISTS fk_atv_value;
ALTER TABLE IF EXISTS ONLY public.attribute DROP CONSTRAINT IF EXISTS fk_attribute_config;
ALTER TABLE IF EXISTS ONLY public.value DROP CONSTRAINT IF EXISTS pk_value;
ALTER TABLE IF EXISTS ONLY public.user_permission DROP CONSTRAINT IF EXISTS pk_user_permission;
ALTER TABLE IF EXISTS ONLY public.link_type DROP CONSTRAINT IF EXISTS pk_link_type;
ALTER TABLE IF EXISTS ONLY public.header DROP CONSTRAINT IF EXISTS pk_header;
ALTER TABLE IF EXISTS ONLY public.district_value DROP CONSTRAINT IF EXISTS pk_district_value;
ALTER TABLE IF EXISTS ONLY public.district DROP CONSTRAINT IF EXISTS pk_district;
ALTER TABLE IF EXISTS ONLY public.depth DROP CONSTRAINT IF EXISTS pk_depth;
ALTER TABLE IF EXISTS ONLY public.configuration DROP CONSTRAINT IF EXISTS pk_configuration;
ALTER TABLE IF EXISTS ONLY public.building_link DROP CONSTRAINT IF EXISTS pk_building_link;
ALTER TABLE IF EXISTS ONLY public.building DROP CONSTRAINT IF EXISTS pk_building;
ALTER TABLE IF EXISTS ONLY public.building_property DROP CONSTRAINT IF EXISTS pk_bp;
ALTER TABLE IF EXISTS ONLY public.building_value DROP CONSTRAINT IF EXISTS pk_attribute_building_value;
ALTER TABLE IF EXISTS ONLY public.attribute DROP CONSTRAINT IF EXISTS pk_attribute;
ALTER TABLE IF EXISTS ONLY public.app_user DROP CONSTRAINT IF EXISTS pk_app_user;
ALTER TABLE IF EXISTS public.value ALTER COLUMN id DROP DEFAULT;
ALTER TABLE IF EXISTS public.user_permission ALTER COLUMN id DROP DEFAULT;
ALTER TABLE IF EXISTS public.link_type ALTER COLUMN id DROP DEFAULT;
ALTER TABLE IF EXISTS public.header ALTER COLUMN id DROP DEFAULT;
ALTER TABLE IF EXISTS public.district_value ALTER COLUMN id DROP DEFAULT;
ALTER TABLE IF EXISTS public.district ALTER COLUMN id DROP DEFAULT;
ALTER TABLE IF EXISTS public.depth ALTER COLUMN id DROP DEFAULT;
ALTER TABLE IF EXISTS public.configuration ALTER COLUMN id DROP DEFAULT;
ALTER TABLE IF EXISTS public.building_value ALTER COLUMN id DROP DEFAULT;
ALTER TABLE IF EXISTS public.building_link ALTER COLUMN id DROP DEFAULT;
ALTER TABLE IF EXISTS public.building ALTER COLUMN id DROP DEFAULT;
ALTER TABLE IF EXISTS public.attribute ALTER COLUMN id DROP DEFAULT;
ALTER TABLE IF EXISTS public.app_user ALTER COLUMN id DROP DEFAULT;
DROP SEQUENCE IF EXISTS public.value_id_seq;
DROP TABLE IF EXISTS public.value;
DROP SEQUENCE IF EXISTS public.user_permission_id_seq;
DROP TABLE IF EXISTS public.user_permission;
DROP SEQUENCE IF EXISTS public.link_type_id_seq;
DROP TABLE IF EXISTS public.link_type;
DROP SEQUENCE IF EXISTS public.header_id_seq;
DROP TABLE IF EXISTS public.header;
DROP SEQUENCE IF EXISTS public.district_value_id_seq;
DROP TABLE IF EXISTS public.district_value;
DROP SEQUENCE IF EXISTS public.district_id_seq;
DROP TABLE IF EXISTS public.district;
DROP SEQUENCE IF EXISTS public.depth_id_seq;
DROP TABLE IF EXISTS public.depth;
DROP SEQUENCE IF EXISTS public.configuration_id_seq;
DROP TABLE IF EXISTS public.configuration;
DROP SEQUENCE IF EXISTS public.building_value_id_seq;
DROP TABLE IF EXISTS public.building_value;
DROP TABLE IF EXISTS public.building_property;
DROP SEQUENCE IF EXISTS public.building_link_id_seq;
DROP TABLE IF EXISTS public.building_link;
DROP SEQUENCE IF EXISTS public.building_id_seq;
DROP TABLE IF EXISTS public.building;
DROP SEQUENCE IF EXISTS public.attribute_id_seq;
DROP TABLE IF EXISTS public.attribute;
DROP SEQUENCE IF EXISTS public.app_user_id_seq;
DROP TABLE IF EXISTS public.app_user;

--
-- Name: app_user; Type: TABLE; Schema: public; Owner: test
--

CREATE TABLE public.app_user (
                                 id bigint NOT NULL,
                                 firstname character varying(50) NOT NULL,
                                 lastname character varying(50) NOT NULL,
                                 id_sso character varying(50) NOT NULL
);


ALTER TABLE public.app_user OWNER TO test;

--
-- Name: app_user_id_seq; Type: SEQUENCE; Schema: public; Owner: test
--

CREATE SEQUENCE public.app_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.app_user_id_seq OWNER TO test;

--
-- Name: app_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: test
--

ALTER SEQUENCE public.app_user_id_seq OWNED BY public.app_user.id;


--
-- Name: attribute; Type: TABLE; Schema: public; Owner: test
--

CREATE TABLE public.attribute (
                                  id bigint NOT NULL,
                                  id_configuration bigint NOT NULL,
                                  name character varying(50) NOT NULL
);


ALTER TABLE public.attribute OWNER TO test;

--
-- Name: attribute_id_seq; Type: SEQUENCE; Schema: public; Owner: test
--

CREATE SEQUENCE public.attribute_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.attribute_id_seq OWNER TO test;

--
-- Name: attribute_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: test
--

ALTER SEQUENCE public.attribute_id_seq OWNED BY public.attribute.id;


--
-- Name: building; Type: TABLE; Schema: public; Owner: test
--

CREATE TABLE public.building (
                                 id bigint NOT NULL,
                                 id_district bigint,
                                 id_building_value bigint NOT NULL,
                                 id_key bigint NOT NULL,
                                 size integer,
                                 color character varying(50),
                                 x_coordinate double precision NOT NULL,
                                 y_coordinate double precision NOT NULL,
                                 comment character varying(50)
);


ALTER TABLE public.building OWNER TO test;

--
-- Name: building_id_seq; Type: SEQUENCE; Schema: public; Owner: test
--

CREATE SEQUENCE public.building_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.building_id_seq OWNER TO test;

--
-- Name: building_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: test
--

ALTER SEQUENCE public.building_id_seq OWNED BY public.building.id;


--
-- Name: building_link; Type: TABLE; Schema: public; Owner: test
--

CREATE TABLE public.building_link (
                                      id bigint NOT NULL,
                                      id_building_1 bigint NOT NULL,
                                      id_link_type bigint NOT NULL,
                                      id_building_2 bigint NOT NULL,
                                      color character varying(50),
                                      thickness double precision,
                                      line_type integer
);


ALTER TABLE public.building_link OWNER TO test;

--
-- Name: building_link_id_seq; Type: SEQUENCE; Schema: public; Owner: test
--

CREATE SEQUENCE public.building_link_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.building_link_id_seq OWNER TO test;

--
-- Name: building_link_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: test
--

ALTER SEQUENCE public.building_link_id_seq OWNED BY public.building_link.id;


--
-- Name: building_property; Type: TABLE; Schema: public; Owner: test
--

CREATE TABLE public.building_property (
                                          id_value bigint NOT NULL,
                                          id_building bigint NOT NULL
);


ALTER TABLE public.building_property OWNER TO test;

--
-- Name: building_value; Type: TABLE; Schema: public; Owner: test
--

CREATE TABLE public.building_value (
                                       id bigint NOT NULL,
                                       size integer NOT NULL,
                                       id_value bigint NOT NULL,
                                       color character varying(50) NOT NULL,
                                       icon character varying(50) NOT NULL
);


ALTER TABLE public.building_value OWNER TO test;

--
-- Name: building_value_id_seq; Type: SEQUENCE; Schema: public; Owner: test
--

CREATE SEQUENCE public.building_value_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.building_value_id_seq OWNER TO test;

--
-- Name: building_value_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: test
--

ALTER SEQUENCE public.building_value_id_seq OWNED BY public.building_value.id;


--
-- Name: configuration; Type: TABLE; Schema: public; Owner: test
--

CREATE TABLE public.configuration (
                                      id bigint NOT NULL,
                                      name character varying(50) NOT NULL,
                                      id_owner bigint NOT NULL,
                                      building_attribute character varying(50) NOT NULL,
                                      building_color_attribute character varying(50) NOT NULL
);


ALTER TABLE public.configuration OWNER TO test;

--
-- Name: configuration_id_seq; Type: SEQUENCE; Schema: public; Owner: test
--

CREATE SEQUENCE public.configuration_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.configuration_id_seq OWNER TO test;

--
-- Name: configuration_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: test
--

ALTER SEQUENCE public.configuration_id_seq OWNED BY public.configuration.id;


--
-- Name: depth; Type: TABLE; Schema: public; Owner: test
--

CREATE TABLE public.depth (
                              id bigint NOT NULL,
                              id_configuration bigint NOT NULL,
                              id_attribute bigint NOT NULL,
                              depth integer NOT NULL
);


ALTER TABLE public.depth OWNER TO test;

--
-- Name: depth_id_seq; Type: SEQUENCE; Schema: public; Owner: test
--

CREATE SEQUENCE public.depth_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.depth_id_seq OWNER TO test;

--
-- Name: depth_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: test
--

ALTER SEQUENCE public.depth_id_seq OWNED BY public.depth.id;


--
-- Name: district; Type: TABLE; Schema: public; Owner: test
--

CREATE TABLE public.district (
                                 id bigint NOT NULL,
                                 id_parent bigint,
                                 id_district_value bigint NOT NULL,
                                 size integer,
                                 x_coordinate double precision NOT NULL,
                                 y_coordinate double precision NOT NULL,
                                 comment character varying(50),
                                 color character varying(50),
                                 icon character varying(50)
);


ALTER TABLE public.district OWNER TO test;

--
-- Name: district_id_seq; Type: SEQUENCE; Schema: public; Owner: test
--

CREATE SEQUENCE public.district_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.district_id_seq OWNER TO test;

--
-- Name: district_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: test
--

ALTER SEQUENCE public.district_id_seq OWNED BY public.district.id;


--
-- Name: district_value; Type: TABLE; Schema: public; Owner: test
--

CREATE TABLE public.district_value (
                                       id bigint NOT NULL,
                                       size integer NOT NULL,
                                       id_value bigint NOT NULL,
                                       color character varying(50) NOT NULL,
                                       icon character varying(50) NOT NULL
);


ALTER TABLE public.district_value OWNER TO test;

--
-- Name: district_value_id_seq; Type: SEQUENCE; Schema: public; Owner: test
--

CREATE SEQUENCE public.district_value_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.district_value_id_seq OWNER TO test;

--
-- Name: district_value_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: test
--

ALTER SEQUENCE public.district_value_id_seq OWNED BY public.district_value.id;


--
-- Name: header; Type: TABLE; Schema: public; Owner: test
--

CREATE TABLE public.header (
                               id bigint NOT NULL,
                               name character varying(50) NOT NULL,
                               id_configuration bigint NOT NULL,
                               id_last_user bigint NOT NULL,
                               creation_date date NOT NULL,
                               last_update date NOT NULL,
                               miniature character varying(50)
);


ALTER TABLE public.header OWNER TO test;

--
-- Name: header_id_seq; Type: SEQUENCE; Schema: public; Owner: test
--

CREATE SEQUENCE public.header_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.header_id_seq OWNER TO test;

--
-- Name: header_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: test
--

ALTER SEQUENCE public.header_id_seq OWNED BY public.header.id;


--
-- Name: link_type; Type: TABLE; Schema: public; Owner: test
--

CREATE TABLE public.link_type (
                                  id bigint NOT NULL,
                                  id_configuration bigint NOT NULL,
                                  color character varying(50) NOT NULL,
                                  thickness double precision NOT NULL,
                                  line_type integer NOT NULL,
                                  id_attribute_1 bigint NOT NULL,
                                  id_attribute_2 bigint NOT NULL
);


ALTER TABLE public.link_type OWNER TO test;

--
-- Name: link_type_id_seq; Type: SEQUENCE; Schema: public; Owner: test
--

CREATE SEQUENCE public.link_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.link_type_id_seq OWNER TO test;

--
-- Name: link_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: test
--

ALTER SEQUENCE public.link_type_id_seq OWNED BY public.link_type.id;


--
-- Name: user_permission; Type: TABLE; Schema: public; Owner: test
--

CREATE TABLE public.user_permission (
                                        id bigint NOT NULL,
                                        permission character varying(50) NOT NULL,
                                        id_header bigint NOT NULL,
                                        id_user bigint NOT NULL
);


ALTER TABLE public.user_permission OWNER TO test;

--
-- Name: user_permission_id_seq; Type: SEQUENCE; Schema: public; Owner: test
--

CREATE SEQUENCE public.user_permission_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_permission_id_seq OWNER TO test;

--
-- Name: user_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: test
--

ALTER SEQUENCE public.user_permission_id_seq OWNED BY public.user_permission.id;


--
-- Name: value; Type: TABLE; Schema: public; Owner: test
--

CREATE TABLE public.value (
                              id bigint NOT NULL,
                              id_attribute bigint NOT NULL,
                              name character varying(255) NOT NULL
);


ALTER TABLE public.value OWNER TO test;

--
-- Name: value_id_seq; Type: SEQUENCE; Schema: public; Owner: test
--

CREATE SEQUENCE public.value_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.value_id_seq OWNER TO test;

--
-- Name: value_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: test
--

ALTER SEQUENCE public.value_id_seq OWNED BY public.value.id;


--
-- Name: app_user id; Type: DEFAULT; Schema: public; Owner: test
--

ALTER TABLE ONLY public.app_user ALTER COLUMN id SET DEFAULT nextval('public.app_user_id_seq'::regclass);


--
-- Name: attribute id; Type: DEFAULT; Schema: public; Owner: test
--

ALTER TABLE ONLY public.attribute ALTER COLUMN id SET DEFAULT nextval('public.attribute_id_seq'::regclass);


--
-- Name: building id; Type: DEFAULT; Schema: public; Owner: test
--

ALTER TABLE ONLY public.building ALTER COLUMN id SET DEFAULT nextval('public.building_id_seq'::regclass);


--
-- Name: building_link id; Type: DEFAULT; Schema: public; Owner: test
--

ALTER TABLE ONLY public.building_link ALTER COLUMN id SET DEFAULT nextval('public.building_link_id_seq'::regclass);


--
-- Name: building_value id; Type: DEFAULT; Schema: public; Owner: test
--

ALTER TABLE ONLY public.building_value ALTER COLUMN id SET DEFAULT nextval('public.building_value_id_seq'::regclass);


--
-- Name: configuration id; Type: DEFAULT; Schema: public; Owner: test
--

ALTER TABLE ONLY public.configuration ALTER COLUMN id SET DEFAULT nextval('public.configuration_id_seq'::regclass);


--
-- Name: depth id; Type: DEFAULT; Schema: public; Owner: test
--

ALTER TABLE ONLY public.depth ALTER COLUMN id SET DEFAULT nextval('public.depth_id_seq'::regclass);


--
-- Name: district id; Type: DEFAULT; Schema: public; Owner: test
--

ALTER TABLE ONLY public.district ALTER COLUMN id SET DEFAULT nextval('public.district_id_seq'::regclass);


--
-- Name: district_value id; Type: DEFAULT; Schema: public; Owner: test
--

ALTER TABLE ONLY public.district_value ALTER COLUMN id SET DEFAULT nextval('public.district_value_id_seq'::regclass);


--
-- Name: header id; Type: DEFAULT; Schema: public; Owner: test
--

ALTER TABLE ONLY public.header ALTER COLUMN id SET DEFAULT nextval('public.header_id_seq'::regclass);


--
-- Name: link_type id; Type: DEFAULT; Schema: public; Owner: test
--

ALTER TABLE ONLY public.link_type ALTER COLUMN id SET DEFAULT nextval('public.link_type_id_seq'::regclass);


--
-- Name: user_permission id; Type: DEFAULT; Schema: public; Owner: test
--

ALTER TABLE ONLY public.user_permission ALTER COLUMN id SET DEFAULT nextval('public.user_permission_id_seq'::regclass);


--
-- Name: value id; Type: DEFAULT; Schema: public; Owner: test
--

ALTER TABLE ONLY public.value ALTER COLUMN id SET DEFAULT nextval('public.value_id_seq'::regclass);

--
-- Name: app_user pk_app_user; Type: CONSTRAINT; Schema: public; Owner: test
--

ALTER TABLE ONLY public.app_user
    ADD CONSTRAINT pk_app_user PRIMARY KEY (id);


--
-- Name: attribute pk_attribute; Type: CONSTRAINT; Schema: public; Owner: test
--

ALTER TABLE ONLY public.attribute
    ADD CONSTRAINT pk_attribute PRIMARY KEY (id);


--
-- Name: building_value pk_attribute_building_value; Type: CONSTRAINT; Schema: public; Owner: test
--

ALTER TABLE ONLY public.building_value
    ADD CONSTRAINT pk_attribute_building_value PRIMARY KEY (id);


--
-- Name: building_property pk_bp; Type: CONSTRAINT; Schema: public; Owner: test
--

ALTER TABLE ONLY public.building_property
    ADD CONSTRAINT pk_bp PRIMARY KEY (id_value, id_building);


--
-- Name: building pk_building; Type: CONSTRAINT; Schema: public; Owner: test
--

ALTER TABLE ONLY public.building
    ADD CONSTRAINT pk_building PRIMARY KEY (id);


--
-- Name: building_link pk_building_link; Type: CONSTRAINT; Schema: public; Owner: test
--

ALTER TABLE ONLY public.building_link
    ADD CONSTRAINT pk_building_link PRIMARY KEY (id);


--
-- Name: configuration pk_configuration; Type: CONSTRAINT; Schema: public; Owner: test
--

ALTER TABLE ONLY public.configuration
    ADD CONSTRAINT pk_configuration PRIMARY KEY (id);


--
-- Name: depth pk_depth; Type: CONSTRAINT; Schema: public; Owner: test
--

ALTER TABLE ONLY public.depth
    ADD CONSTRAINT pk_depth PRIMARY KEY (id);


--
-- Name: district pk_district; Type: CONSTRAINT; Schema: public; Owner: test
--

ALTER TABLE ONLY public.district
    ADD CONSTRAINT pk_district PRIMARY KEY (id);


--
-- Name: district_value pk_district_value; Type: CONSTRAINT; Schema: public; Owner: test
--

ALTER TABLE ONLY public.district_value
    ADD CONSTRAINT pk_district_value PRIMARY KEY (id);


--
-- Name: header pk_header; Type: CONSTRAINT; Schema: public; Owner: test
--

ALTER TABLE ONLY public.header
    ADD CONSTRAINT pk_header PRIMARY KEY (id);


--
-- Name: link_type pk_link_type; Type: CONSTRAINT; Schema: public; Owner: test
--

ALTER TABLE ONLY public.link_type
    ADD CONSTRAINT pk_link_type PRIMARY KEY (id);


--
-- Name: user_permission pk_user_permission; Type: CONSTRAINT; Schema: public; Owner: test
--

ALTER TABLE ONLY public.user_permission
    ADD CONSTRAINT pk_user_permission PRIMARY KEY (id);


--
-- Name: value pk_value; Type: CONSTRAINT; Schema: public; Owner: test
--

ALTER TABLE ONLY public.value
    ADD CONSTRAINT pk_value PRIMARY KEY (id);


--
-- Name: attribute fk_attribute_config; Type: FK CONSTRAINT; Schema: public; Owner: test
--

ALTER TABLE ONLY public.attribute
    ADD CONSTRAINT fk_attribute_config FOREIGN KEY (id_configuration) REFERENCES public.configuration(id);


--
-- Name: building_value fk_atv_value; Type: FK CONSTRAINT; Schema: public; Owner: test
--

ALTER TABLE ONLY public.building_value
    ADD CONSTRAINT fk_atv_value FOREIGN KEY (id_value) REFERENCES public.value(id);


--
-- Name: building_link fk_bl_b1; Type: FK CONSTRAINT; Schema: public; Owner: test
--

ALTER TABLE ONLY public.building_link
    ADD CONSTRAINT fk_bl_b1 FOREIGN KEY (id_building_1) REFERENCES public.building(id);


--
-- Name: building_link fk_bl_b2; Type: FK CONSTRAINT; Schema: public; Owner: test
--

ALTER TABLE ONLY public.building_link
    ADD CONSTRAINT fk_bl_b2 FOREIGN KEY (id_building_2) REFERENCES public.building(id);


--
-- Name: building_link fk_bl_lt; Type: FK CONSTRAINT; Schema: public; Owner: test
--

ALTER TABLE ONLY public.building_link
    ADD CONSTRAINT fk_bl_lt FOREIGN KEY (id_link_type) REFERENCES public.link_type(id);


--
-- Name: building_property fk_bp_building; Type: FK CONSTRAINT; Schema: public; Owner: test
--

ALTER TABLE ONLY public.building_property
    ADD CONSTRAINT fk_bp_building FOREIGN KEY (id_building) REFERENCES public.building(id);


--
-- Name: building_property fk_bp_value; Type: FK CONSTRAINT; Schema: public; Owner: test
--

ALTER TABLE ONLY public.building_property
    ADD CONSTRAINT fk_bp_value FOREIGN KEY (id_value) REFERENCES public.value(id);


--
-- Name: building fk_building_bv; Type: FK CONSTRAINT; Schema: public; Owner: test
--

ALTER TABLE ONLY public.building
    ADD CONSTRAINT fk_building_bv FOREIGN KEY (id_building_value) REFERENCES public.building_value(id);


--
-- Name: building fk_building_district; Type: FK CONSTRAINT; Schema: public; Owner: test
--

ALTER TABLE ONLY public.building
    ADD CONSTRAINT fk_building_district FOREIGN KEY (id_district) REFERENCES public.district(id);

--
-- Name: building fk_building_district; Type: FK CONSTRAINT; Schema: public; Owner: test
--

ALTER TABLE ONLY public.building
    ADD CONSTRAINT fk_building_key FOREIGN KEY (id_key) REFERENCES public.value(id);


--
-- Name: configuration fk_config_owner; Type: FK CONSTRAINT; Schema: public; Owner: test
--

ALTER TABLE ONLY public.configuration
    ADD CONSTRAINT fk_config_owner FOREIGN KEY (id_owner) REFERENCES public.app_user(id);


--
-- Name: depth fk_depth_attribute; Type: FK CONSTRAINT; Schema: public; Owner: test
--

ALTER TABLE ONLY public.depth
    ADD CONSTRAINT fk_depth_attribute FOREIGN KEY (id_attribute) REFERENCES public.attribute(id);


--
-- Name: depth fk_depth_config; Type: FK CONSTRAINT; Schema: public; Owner: test
--

ALTER TABLE ONLY public.depth
    ADD CONSTRAINT fk_depth_config FOREIGN KEY (id_configuration) REFERENCES public.configuration(id);


--
-- Name: district fk_district_district; Type: FK CONSTRAINT; Schema: public; Owner: test
--

ALTER TABLE ONLY public.district
    ADD CONSTRAINT fk_district_district FOREIGN KEY (id_parent) REFERENCES public.district(id);


--
-- Name: district fk_district_dv; Type: FK CONSTRAINT; Schema: public; Owner: test
--

ALTER TABLE ONLY public.district
    ADD CONSTRAINT fk_district_dv FOREIGN KEY (id_district_value) REFERENCES public.district_value(id);


--
-- Name: district_value fk_dv_value; Type: FK CONSTRAINT; Schema: public; Owner: test
--

ALTER TABLE ONLY public.district_value
    ADD CONSTRAINT fk_dv_value FOREIGN KEY (id_value) REFERENCES public.value(id);


--
-- Name: header fk_header_config; Type: FK CONSTRAINT; Schema: public; Owner: test
--

ALTER TABLE ONLY public.header
    ADD CONSTRAINT fk_header_config FOREIGN KEY (id_configuration) REFERENCES public.configuration(id);


--
-- Name: header fk_header_user; Type: FK CONSTRAINT; Schema: public; Owner: test
--

ALTER TABLE ONLY public.header
    ADD CONSTRAINT fk_header_user FOREIGN KEY (id_last_user) REFERENCES public.app_user(id);


--
-- Name: link_type fk_lt_a1; Type: FK CONSTRAINT; Schema: public; Owner: test
--

ALTER TABLE ONLY public.link_type
    ADD CONSTRAINT fk_lt_a1 FOREIGN KEY (id_attribute_1) REFERENCES public.attribute(id);


--
-- Name: link_type fk_lt_a2; Type: FK CONSTRAINT; Schema: public; Owner: test
--

ALTER TABLE ONLY public.link_type
    ADD CONSTRAINT fk_lt_a2 FOREIGN KEY (id_attribute_2) REFERENCES public.attribute(id);


--
-- Name: link_type fk_lt_config; Type: FK CONSTRAINT; Schema: public; Owner: test
--

ALTER TABLE ONLY public.link_type
    ADD CONSTRAINT fk_lt_config FOREIGN KEY (id_configuration) REFERENCES public.configuration(id);


--
-- Name: user_permission fk_permission_header; Type: FK CONSTRAINT; Schema: public; Owner: test
--

ALTER TABLE ONLY public.user_permission
    ADD CONSTRAINT fk_permission_header FOREIGN KEY (id_header) REFERENCES public.header(id);


--
-- Name: user_permission fk_permission_user; Type: FK CONSTRAINT; Schema: public; Owner: test
--

ALTER TABLE ONLY public.user_permission
    ADD CONSTRAINT fk_permission_user FOREIGN KEY (id_user) REFERENCES public.app_user(id);


--
-- Name: value fk_value_attribute; Type: FK CONSTRAINT; Schema: public; Owner: test
--

ALTER TABLE ONLY public.value
    ADD CONSTRAINT fk_value_attribute FOREIGN KEY (id_attribute) REFERENCES public.attribute(id);


--
-- PostgreSQL database dump complete
--

